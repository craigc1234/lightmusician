//
//  BLEManager.swift
//
//  Created by Craig Conner on 1/24/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import Foundation
import CoreBluetooth


let kScanStartedNotification = Notification.Name(rawValue:"ScanStartedNotification")
let kScanStoppedNotification = Notification.Name(rawValue:"ScanStoppedNotification")

//peripheral discovered
let kPeripheralFoundNotification = Notification.Name(rawValue:"PeripheralFoundNotification")

//peripheral connected to, services and characteristics discovered
let kConnectionStartedNotification = Notification.Name(rawValue:"ConnectionStartedNotification")
let kPeripheralConnectedNotification = Notification.Name(rawValue:"PeripheralConnectedNotification")
let kCharacteristicsFoundNotification = Notification.Name(rawValue:"CharacteristicsFoundNotification")
let kPeripheralDisconnectedNotification = Notification.Name(rawValue:"PeripheralDisconnectedNotification")

let kWriteOutputNotification = Notification.Name(rawValue:"WriteOutputNotification")

let kPeripheralArrayUpdatedNotification = Notification.Name(rawValue:"kPeripheralArrayUpdatedNotification")


class BLEManager: NSObject, CBCentralManagerDelegate {
    
    static var bleManager:BLEManager?
    var centralManager: CBCentralManager?
    
    //When we find a peripheral for the first time, we just keep that in the transient peripherals dict/array
    //It would be nice if the UUID were a CBUUID, but CBPeripheral has a UUID, so....
    //one to enumerate them, one for lookup
    var peripheralDict =  [UUID : BLEPeripheral]()
    var peripheralArray = [BLEPeripheral]()
    
    var currentTimer:Timer?
    var interval:TimeInterval = 30
    
    class func getPeripheral(peripheralID:UUID) -> BLEPeripheral? {
        return BLEManager.bleManager?.peripheralDict[peripheralID]
    }
    
    class func getCharacteristic(peripheralID:UUID, serviceID:CBUUID, characteristicID:CBUUID) -> CBCharacteristic? {
        if let peripheral = BLEManager.bleManager?.peripheralDict[peripheralID] {
            return peripheral.getCharacteristicByID(serviceID: serviceID, charID: characteristicID)
        }
        return nil
    }
    
    override init() {
        super.init()
        BLEManager.bleManager = self
        let centralQueue = DispatchQueue(label: AppDelegate.dispatchQueueName, attributes: [])
        centralManager = CBCentralManager(delegate: self, queue: centralQueue)
    }
    
    func getPeripheral(uuid:UUID) -> BLEPeripheral? {
        if let peripheral = peripheralDict[uuid] {
            return peripheral
        }
        return nil
    }
    
    func getPeripheral(byIndex:Int) -> BLEPeripheral? {
        if peripheralArray.count > byIndex {
            return peripheralArray[byIndex]
        }
        return nil
    }
    
    func removePeripheral(uuid:UUID) {
        if let peripheral = peripheralDict[uuid] {
            diconnectFromPeripheral(blePeripheral: peripheral)
                peripheralDict.removeValue(forKey: peripheral.getID()!)
        }
    }
    
    func removePeripheral(byIndex:Int) {
        if peripheralArray.count > byIndex {
            let peripheral = peripheralArray.remove(at: byIndex)
            peripheralDict.removeValue(forKey: (peripheral.getID())!)
        }
    }

    
    //scan for peripherals
    // if interval is default 15, assume first run, else assume we are trying to extend scan time
    //      so stop timer and restart with new value
    func scan(uuid:[CBUUID]? = nil) {
        if let central = centralManager {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kScanStartedNotification, object: nil, userInfo: nil)
            }
            peripheralDict =  [UUID : BLEPeripheral]()
            peripheralArray = [BLEPeripheral]()
                central.scanForPeripherals(withServices: uuid, options: nil)

            currentTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(BLEManager.endScan), userInfo: nil, repeats: false)
        }
    }
    
    func endScan() {
        currentTimer?.invalidate()
        currentTimer = nil
        if let central = centralManager {
            central.stopScan()
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kScanStoppedNotification, object: nil, userInfo: nil)
            }
        }
    }
    
    func connectToPeripheral(blePeripheral:BLEPeripheral) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kConnectionStartedNotification, object: blePeripheral, userInfo: nil)
        }
        centralManager?.connect(blePeripheral.peripheral, options: nil)
    }
    
    func diconnectFromPeripheral(blePeripheral:BLEPeripheral) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kPeripheralDisconnectedNotification, object: blePeripheral, userInfo: nil)
        }
        if let peripheral = peripheralDict[blePeripheral.peripheral.identifier] {
            peripheral.connected = false
        }
        centralManager?.cancelPeripheralConnection(blePeripheral.peripheral)
    }
    
    // MARK: - CBCentralManagerDelegate
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print(peripheral.name, peripheral.identifier)
        // Be sure to retain the peripheral or it will fail during connection.
        
        if peripheralDict[peripheral.identifier] == nil {
           let blePeripheral = BLEPeripheral(initWithPeripheral: peripheral, advertisementData: advertisementData, rssi: RSSI)
            peripheralDict[peripheral.identifier] = blePeripheral
            peripheralArray.append(blePeripheral)
        }
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kPeripheralFoundNotification, object: nil, userInfo: nil)
        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.readRSSI()
        peripheral.discoverServices(nil)
        if let sensor = peripheralDict[peripheral.identifier] {
            sensor.connected = true
            print("Connected")
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kPeripheralConnectedNotification, object: nil, userInfo: nil)
            }
            
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?)    {
        //methinks this doth disconnects too much
        print("Disconnecting")
        if let peripheral = peripheralDict[peripheral.identifier] {
            if peripheral.connected == true {
                connectToPeripheral(blePeripheral: peripheral)
            }
        }
        
    }
    
    func clearDevice(peripheral : BLEPeripheral) {
        peripheralDict.removeValue(forKey: peripheral.getID()!)
    }
    
    
    func clearDevices() {
        peripheralDict = [UUID : BLEPeripheral]()
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state) {
        case .poweredOff:
            self.clearDevices()
            //       showAlertWithText("Error", message: "Bluetooth is off. To use this app turn it on in the Settings.")
            break
        case .unauthorized:
            //        showAlertWithText("Error", message: "This app is not supported on this device.")
            break
        case .unknown:
            // Wait for another event
            break
        case .poweredOn:
           // self.scan()
            break
        case .resetting:
            self.clearDevices()
            
        case .unsupported:
            break
        }
    }
}
