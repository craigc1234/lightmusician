//
//  BLEPeripheral.swift
//  DanceParty
//
//  Created by Craig Conner on 6/6/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import Foundation
import CoreBluetooth

enum ActionType:Int32 {
    case write = 1
    case respond = 2
    case read = 4
    case notify = 8
}

class BLEPeripheral: NSObject, CBPeripheralDelegate {
    
    // MARK: Variables
    
    
    // This is for serial use right now, might want to make this an array
    /// The characteristic 0xFFE1 we need to write to, of the connectedPeripheral
    var writeCharacteristic : CBCharacteristic?
    
    /// UUID of the service to look for.
    var serialServiceUUID = CBUUID(string: "FFE0")
    
    /// UUID of the characteristic to look for.
    var serialCharacteristicUUID = CBUUID(string: "FFE1")
    
    //With response for the light
    /// Whether to write to the HM10 with or without response. Set automatically.
    /// Legit HM10 modules (from JNHuaMao) require 'Write without Response',
    /// while fake modules (e.g. from Bolutek) require 'Write with Response'.
    private var writeType: CBCharacteristicWriteType = .withResponse
    
    
    
    var peripheral: CBPeripheral
    var rssi:NSNumber?
    var advertisementKeyArray = [String]()
    var advertisementValueArray = [Any]()
    var connected:Bool = false
    var numServices:Int = 0
    
    var servicesArray = [CBService]()
    var serviceToCharacteristicMap = Dictionary<CBUUID, [CBCharacteristic]>()
    var serviceToCharacteristicDictMap = Dictionary<CBUUID, [CBUUID : CBCharacteristic]>()
    
    init(initWithPeripheral newPeripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber ) {
        
        self.peripheral = newPeripheral
        super.init()
        self.peripheral.delegate = self
        
        rssi = RSSI
        //convert so I can index them later
        for (key, value) in advertisementData {
            advertisementKeyArray.append(key)
            advertisementValueArray.append(value)
        }
    }
        
    func startDiscoveringServices() {
        self.peripheral.discoverServices(nil)
    }
    
    func getName() -> String? {
        return peripheral.name
    }
    
    func getID() -> UUID? {
        return peripheral.identifier
    }
        
    func getRSSI() -> NSNumber? {
        return rssi
    }
    
    func getADKeyArray() -> [String] {
        return advertisementKeyArray
    }
    
    func getADValueArray() -> [Any] {
        return advertisementValueArray
    }
    
    func getNumServices() -> Int? {
        if let services = peripheral.services {
            return services.count
        }
        else {
            return servicesArray.count
        }
    }
    
    func getService(index:Int) -> CBService? {
        if getNumServices() != nil && getNumServices()! > index {
            if let services = peripheral.services {
                return services[index]
            }
            else {
                //print("got service from array")
                return servicesArray[index]
            }
        }
        else {
            return nil
        }
    }
        
    func getCharacteristics(forService:CBService) -> [CBCharacteristic]? {
        //   print(forService.uuid)
        if let characteristics = serviceToCharacteristicMap[forService.uuid] {
            return characteristics
        }
        else {
            return []
        }
    }
    
    func getCharacteristic(forService:CBService, byIndex:Int) -> CBCharacteristic? {
        if let characteristics = serviceToCharacteristicMap[forService.uuid] {
            return characteristics[byIndex]
        }
        else {
            return nil
        }
    }
        
    func getCharacteristicByID(serviceID:CBUUID, charID:CBUUID) -> CBCharacteristic? {
        
        let characteristics = serviceToCharacteristicDictMap[serviceID]
        if characteristics != nil {
            return characteristics![charID]
        }
        
        return nil
    }
    
    // Mark: - CBPeripheralDelegate
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        if error == nil {
            rssi = RSSI
            startDiscoveringServices()
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        if (peripheral != self.peripheral) {
            // Wrong Peripheral
            return
        }
        
        if (error != nil) {
            return
        }
        
        if ((peripheral.services == nil) || (peripheral.services!.count == 0)) {
            // No Services
            return
        }
        
        for service in peripheral.services! {
            if servicesArray.contains(service) == false {
                numServices += 1
                servicesArray.append(service)
                peripheral.discoverCharacteristics( nil, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        numServices -= 1
        if (peripheral != self.peripheral) {
            // Wrong Peripheral
            return
        }
        
        if (error != nil) {
            return
        }
        
        var charArray:[CBCharacteristic]?
        var charDict:[CBUUID :CBCharacteristic]?
        if let serviceMap = serviceToCharacteristicMap[service.uuid] {
            charArray = serviceMap
            charDict = serviceToCharacteristicDictMap[service.uuid]
        }
        else {
            charArray = [CBCharacteristic]()
            charDict = [CBUUID : CBCharacteristic]()
        }
        
        if let characteristics = service.characteristics {
            // print("ch \(service.description)")
            for characteristic in characteristics {
                if (charDict?[characteristic.uuid]) == nil {
                    charArray!.append(characteristic)
                    // print("char \(characteristic.description)")
                    charDict?[characteristic.uuid] = characteristic
                    //   setNotify(characteristic: characteristic, value: true)
                }
            }
            serviceToCharacteristicMap[service.uuid] = charArray
            serviceToCharacteristicDictMap[service.uuid] = charDict
            
            serviceToCharacteristicMap[service.uuid] = charArray
            // Send notification that Bluetooth is connected and all required characteristics are discovered
            //all known services have been looked at
            if numServices == 0 {
                DispatchQueue.main.async {
                    let blePeripheral = BLEManager.bleManager?.peripheralDict[peripheral.identifier]
                    blePeripheral?.connected = true
                    NotificationCenter.default.post(name: kCharacteristicsFoundNotification, object: blePeripheral, userInfo: nil)
                    //print("Chars found")
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        //write output - characteristic.value
        if error == nil {
            let blePeripheral = BLEManager.bleManager?.peripheralDict[peripheral.identifier]
            DispatchQueue.main.async() {
                NotificationCenter.default.post(name: kWriteOutputNotification, object: blePeripheral, userInfo: ["characteristic":characteristic as Any])
            }
        }
    }
    
    //if using write with response, this gets called
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        print("response")
    }
    
    //Assume the first service/characteristic is the one to write to, if not specified
    func writeToFirstCharacteristic(value:String, type:ActionType) {
        var writeType = CBCharacteristicWriteType.withoutResponse
        let data = value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        if let characteristic = getService(index: 0)?.characteristics?[0] {
            if type == .respond {
                peripheral.setNotifyValue(true, for: characteristic)
                writeType = CBCharacteristicWriteType.withResponse
            }
            peripheral.writeValue(data!, for: characteristic, type: writeType)
        }
    }
    
    func writeDataToCharacteristic(characteristic:CBCharacteristic, data:Data, type:ActionType) {
        var writeType = CBCharacteristicWriteType.withoutResponse
        if type == .respond {
            peripheral.setNotifyValue(true, for: characteristic)
            writeType = CBCharacteristicWriteType.withResponse
        }
        peripheral.writeValue(data, for: characteristic, type: writeType)
    }
    
    func readCharacteristic(characteristic:CBCharacteristic) {
        peripheral.readValue(for: characteristic)
    }
    
    func writeDescriptor(characteristic:CBCharacteristic) {
        //  peripheral.desc
    }
    
    func setNotify(characteristic:CBCharacteristic, notifyChar: CBCharacteristic, value:Bool) {
        peripheral.setNotifyValue(value, for: characteristic)
        var enableValue:UInt8 = value ? 1 : 0
        let enablyBytes = Data(bytes: &enableValue, count: MemoryLayout<UInt8>.size)
        peripheral.writeValue(enablyBytes, for: notifyChar, type: CBCharacteristicWriteType.withResponse)
    }
    
    func setNotifyByID(characteristicID:CBUUID, notifyCharID: CBUUID, sensorID:CBUUID, value:Bool) {
        let characteristic = (serviceToCharacteristicDictMap[sensorID]?[characteristicID])!
        let notifyChar = (serviceToCharacteristicDictMap[sensorID]?[notifyCharID])!
        peripheral.setNotifyValue(value, for: characteristic)
        var enableValue:UInt8 = value ? 1 : 0
        let enablyBytes = Data(bytes: &enableValue, count: MemoryLayout<UInt8>.size)
        peripheral.writeValue(enablyBytes, for: notifyChar, type: CBCharacteristicWriteType.withResponse)
    }
        
}
