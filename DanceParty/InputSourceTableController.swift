//
//  InputSourceTableController.swift
//  LightMusician
//
//  Created by Aardvark on 1/16/18.
//  Copyright © 2018 LogicCascade. All rights reserved.
//

import UIKit
import MediaPlayer

class InputSourceTableController: UITableViewController, MPMediaPickerControllerDelegate {

    var mediaPicker: MPMediaPickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mediaPicker = MPMediaPickerController(mediaTypes: .music)
        mediaPicker?.allowsPickingMultipleItems = false
        mediaPicker?.showsCloudItems = false
        mediaPicker?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            SpectralViewController.soundSource.input = .microphone
            self.dismiss(animated: true, completion: nil)
        default:
            SpectralViewController.soundSource.input = .file
            let navController = self.navigationController
            navController?.pushViewController(mediaPicker!, animated: true)
        }
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        SpectralViewController.soundSource.item = mediaItemCollection.items[0]
        mediaPicker.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "unwindToMainViewSegueID", sender: self)
    }
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        SpectralViewController.soundSource.input = .microphone
        mediaPicker.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "unwindToMainViewSegueID", sender: self)
    }
}
