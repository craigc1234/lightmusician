//
//  AppDelegate.swift
//  DanceParty
//
//  Created by Craig Conner on 6/2/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: NSObject, UIApplicationDelegate {
    
    static var appDelegate: AppDelegate?
    @IBOutlet var window: UIWindow?
    @IBOutlet var view: EAGLView!
    
    static var bleManager: BLEManager?
    static var bleCommander: BLECommands?
    static var blePeripheralInterface: BLEPeripheralInterface?

    
    static let modelName = "LightMusician"
    static let dispatchQueueName = "com.logiccascade"
    
    static var managedObjectContext: NSManagedObjectContext?
    static var rootDataObject: Root?
    static var rootCreated = false
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        
        //redirct print as needed
        //let path: NSString = ...;
        //freopen(path.UTF8String, "a+", stdout)
        
        AppDelegate.appDelegate = self
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.tintColor = Color.lightText.value
        navigationBarAppearance.barTintColor = Color.theme.value
        navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.lightText.value]
        UIWindow(frame: UIScreen.main.bounds).tintColor = UIColor.white

        AppDelegate.managedObjectContext = self.persistentContainer.viewContext
        AppDelegate.rootDataObject = getOrCreateRootObject()
        AppDelegate.rootDataObject!.initializeObject()

        saveContext()
        // Turn off the idle timer, since this app doesn't rely on constant touch input
        application.isIdleTimerDisabled = true
        
        window?.makeKeyAndVisible()
        
        AppDelegate.bleManager = BLEManager()
        AppDelegate.bleManager?.reconnectWhenLost = true
        AppDelegate.bleCommander = BLECommands()
        AppDelegate.blePeripheralInterface = BLEPeripheralInterface()
    }
    
    func getOrCreateRootObject() -> Root {
        
        let rootFetcher = NSFetchRequest<NSFetchRequestResult>(entityName: "Root")
        
        do {
            if let fetchedRoot = try AppDelegate.managedObjectContext?.fetch(rootFetcher) as? [Root] {
                if fetchedRoot.count > 0 {
                    return fetchedRoot[0]
                }
            }
            
            let root = Root(context: AppDelegate.managedObjectContext!)
            root.isLighting = true
            root.connectionTypeTrixy = false
            let trixySettings = LightSettings(context: AppDelegate.managedObjectContext!)
            trixySettings.initializeObject(true)
            root.addToTrixySettings(trixySettings)
            let lightSettings = LightSettings(context: AppDelegate.managedObjectContext!)
            lightSettings.initializeObject()
            root.addToLightSettings(lightSettings)
            AppDelegate.rootCreated = true
            
#if targetEnvironment(simulator)
            let light = Light(context: AppDelegate.managedObjectContext!)
            light.name = "Light 1"
            lightSettings.addToLights(light)
            var light1 = Light(context: AppDelegate.managedObjectContext!)
            light1.name = "Light 2"
            lightSettings.addToLights(light1)
            light1 = Light(context: AppDelegate.managedObjectContext!)
            light1.name = "Light 3"
            lightSettings.addToLights(light1)
            light1 = Light(context: AppDelegate.managedObjectContext!)
            light1.name = "Light 4"
            lightSettings.addToLights(light1)
#endif
            
            return root
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate.
            //You should not use this function in a shipping application, although it
            //may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        SpectralViewController.spectralViewController?.playState = true
        SpectralViewController.spectralViewController?.setPlaying()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        SpectralViewController.spectralViewController?.playState = false
        SpectralViewController.spectralViewController?.setPlaying()
     }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "LightMusician")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
