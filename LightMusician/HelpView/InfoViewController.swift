//
//  InfoViewController.swift
//  LuxTrix
//
//  Created by Aardvark on 8/23/18.
//  Copyright © 2018 Luxapel. All rights reserved.
//

import UIKit
import WebKit

enum HelpItem: Int {
    case intro = 0
    case setup
    case settings
    case about
}

class InfoViewController: UIViewController {

    @IBOutlet weak var infoTypeSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var infoWebView: WKWebView!
    var textFontSize = 140
    var startingViewIndex = 0
    var startingPage: HelpItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoWebView.isOpaque = false
        infoWebView.backgroundColor = Color.lightBackground.value
        infoWebView.layer.cornerRadius = 5
        infoWebView.layer.borderColor = UIColor.white.cgColor
        infoWebView.layer.borderWidth = 1
        textFontSize = 140
        if startingPage != nil {
            startingViewIndex = (startingPage?.rawValue)!
        }

        view.backgroundColor = Color.lightBackground.value
    }
    
    override func viewWillAppear(_ animated: Bool) {
        infoTypeSegmentedControl.selectedSegmentIndex = startingViewIndex
        infoTypeValueChanged(self)
    }
    
    @IBAction func infoTypeValueChanged(_ sender: Any) {
        let url: URL
        switch infoTypeSegmentedControl.selectedSegmentIndex {
        case 0:
            url = Bundle.main.url(forResource: "intro", withExtension: "html")!
        case 1:
            url = Bundle.main.url(forResource: "setup", withExtension: "html")!
        case 2:
            url = Bundle.main.url(forResource: "settings", withExtension: "html")!
        default:
            url = Bundle.main.url(forResource: "about", withExtension: "html")!
        }
        infoWebView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        infoWebView.load(request)
    }
    
    @IBAction func fontSizeChanged(_ sender: Any) {
        if let button = sender as? UIButton {
            textFontSize += button.tag * 20
            let jsString = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(textFontSize)%'"
            infoWebView.evaluateJavaScript(jsString, completionHandler: nil)
        }
    }
}
