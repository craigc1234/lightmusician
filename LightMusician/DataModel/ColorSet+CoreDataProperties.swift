//
//  ColorSet+CoreDataProperties.swift
//  
//
//  Created by Aardvark on 3/3/19.
//
//

import Foundation
import CoreData


extension ColorSet {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ColorSet> {
        return NSFetchRequest<ColorSet>(entityName: "ColorSet")
    }

    @NSManaged public var blue: Int16
    @NSManaged public var green: Int16
    @NSManaged public var red: Int16
    @NSManaged public var white: Int16
    @NSManaged public var group: LightSettings?
    @NSManaged public var savedGroup: SavedColorGroup?

}
