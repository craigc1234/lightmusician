//
//  Color+CoreDataClass.swift
//  
//
//  Created by Aardvark on 2/17/19.
//
//

import Foundation
import CoreData
import UIKit

@objc(ColorSet)
public class ColorSet: NSManagedObject {

    func setColor(color: UIColor) {
        var components = (color.cgColor.components)!
        for index in [Int](0..<4) {
            if components[index].isNaN || components[index] < 0 {
                components[index] = 0
            }
        }
        red = Int16(components[0] * 255)
        green = Int16(components[1] * 255)
        blue = Int16(components[2] * 255)
        white = 0
    }
    
    func setColor(red: Int, green: Int, blue: Int, white: Int) {
        self.red = Int16(red)
        self.green = Int16(green)
        self.blue = Int16(blue)
        self.white = Int16(white)
    }
    
    func setColor(fromColorSet: ColorSet) {
        self.red = fromColorSet.red
        self.green = fromColorSet.green
        self.blue = fromColorSet.blue
        self.white = fromColorSet.white
    }
}
