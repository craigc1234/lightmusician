//
//  LightSettings+CoreDataProperties.swift
//  
//
//  Created by Aardvark on 3/6/19.
//
//

import Foundation
import CoreData


extension LightSettings {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LightSettings> {
        return NSFetchRequest<LightSettings>(entityName: "LightSettings")
    }

    @NSManaged public var fade: Float
    @NSManaged public var highFrequency: Int16
    @NSManaged public var lowFrequency: Int16
    @NSManaged public var name: String?
    @NSManaged public var numColors: Int16
    @NSManaged public var suppression: Float
    @NSManaged public var threshold: Float
    @NSManaged public var lightPattern: Int16
    @NSManaged public var numTrixyLights: Int16
    @NSManaged public var colors: NSOrderedSet?
    @NSManaged public var lights: NSOrderedSet?
    @NSManaged public var root: Root?
    @NSManaged public var root1: Root?

}

// MARK: Generated accessors for colors
extension LightSettings {

    @objc(insertObject:inColorsAtIndex:)
    @NSManaged public func insertIntoColors(_ value: ColorSet, at idx: Int)

    @objc(removeObjectFromColorsAtIndex:)
    @NSManaged public func removeFromColors(at idx: Int)

    @objc(insertColors:atIndexes:)
    @NSManaged public func insertIntoColors(_ values: [ColorSet], at indexes: NSIndexSet)

    @objc(removeColorsAtIndexes:)
    @NSManaged public func removeFromColors(at indexes: NSIndexSet)

    @objc(replaceObjectInColorsAtIndex:withObject:)
    @NSManaged public func replaceColors(at idx: Int, with value: ColorSet)

    @objc(replaceColorsAtIndexes:withColors:)
    @NSManaged public func replaceColors(at indexes: NSIndexSet, with values: [ColorSet])

    @objc(addColorsObject:)
    @NSManaged public func addToColors(_ value: ColorSet)

    @objc(removeColorsObject:)
    @NSManaged public func removeFromColors(_ value: ColorSet)

    @objc(addColors:)
    @NSManaged public func addToColors(_ values: NSOrderedSet)

    @objc(removeColors:)
    @NSManaged public func removeFromColors(_ values: NSOrderedSet)

}

// MARK: Generated accessors for lights
extension LightSettings {

    @objc(insertObject:inLightsAtIndex:)
    @NSManaged public func insertIntoLights(_ value: Light, at idx: Int)

    @objc(removeObjectFromLightsAtIndex:)
    @NSManaged public func removeFromLights(at idx: Int)

    @objc(insertLights:atIndexes:)
    @NSManaged public func insertIntoLights(_ values: [Light], at indexes: NSIndexSet)

    @objc(removeLightsAtIndexes:)
    @NSManaged public func removeFromLights(at indexes: NSIndexSet)

    @objc(replaceObjectInLightsAtIndex:withObject:)
    @NSManaged public func replaceLights(at idx: Int, with value: Light)

    @objc(replaceLightsAtIndexes:withLights:)
    @NSManaged public func replaceLights(at indexes: NSIndexSet, with values: [Light])

    @objc(addLightsObject:)
    @NSManaged public func addToLights(_ value: Light)

    @objc(removeLightsObject:)
    @NSManaged public func removeFromLights(_ value: Light)

    @objc(addLights:)
    @NSManaged public func addToLights(_ values: NSOrderedSet)

    @objc(removeLights:)
    @NSManaged public func removeFromLights(_ values: NSOrderedSet)

}
