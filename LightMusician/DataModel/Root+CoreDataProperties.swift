//
//  Root+CoreDataProperties.swift
//  
//
//  Created by Aardvark on 3/6/19.
//
//

import Foundation
import CoreData


extension Root {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Root> {
        return NSFetchRequest<Root>(entityName: "Root")
    }

    @NSManaged public var connectionTypeTrixy: Bool
    @NSManaged public var isLighting: Bool
    @NSManaged public var lightSettings: NSOrderedSet?
    @NSManaged public var savedColors: NSOrderedSet?
    @NSManaged public var trixySettings: NSOrderedSet?

}

// MARK: Generated accessors for lightSettings
extension Root {

    @objc(insertObject:inLightSettingsAtIndex:)
    @NSManaged public func insertIntoLightSettings(_ value: LightSettings, at idx: Int)

    @objc(removeObjectFromLightSettingsAtIndex:)
    @NSManaged public func removeFromLightSettings(at idx: Int)

    @objc(insertLightSettings:atIndexes:)
    @NSManaged public func insertIntoLightSettings(_ values: [LightSettings], at indexes: NSIndexSet)

    @objc(removeLightSettingsAtIndexes:)
    @NSManaged public func removeFromLightSettings(at indexes: NSIndexSet)

    @objc(replaceObjectInLightSettingsAtIndex:withObject:)
    @NSManaged public func replaceLightSettings(at idx: Int, with value: LightSettings)

    @objc(replaceLightSettingsAtIndexes:withLightSettings:)
    @NSManaged public func replaceLightSettings(at indexes: NSIndexSet, with values: [LightSettings])

    @objc(addLightSettingsObject:)
    @NSManaged public func addToLightSettings(_ value: LightSettings)

    @objc(removeLightSettingsObject:)
    @NSManaged public func removeFromLightSettings(_ value: LightSettings)

    @objc(addLightSettings:)
    @NSManaged public func addToLightSettings(_ values: NSOrderedSet)

    @objc(removeLightSettings:)
    @NSManaged public func removeFromLightSettings(_ values: NSOrderedSet)

}

// MARK: Generated accessors for savedColors
extension Root {

    @objc(insertObject:inSavedColorsAtIndex:)
    @NSManaged public func insertIntoSavedColors(_ value: SavedColorGroup, at idx: Int)

    @objc(removeObjectFromSavedColorsAtIndex:)
    @NSManaged public func removeFromSavedColors(at idx: Int)

    @objc(insertSavedColors:atIndexes:)
    @NSManaged public func insertIntoSavedColors(_ values: [SavedColorGroup], at indexes: NSIndexSet)

    @objc(removeSavedColorsAtIndexes:)
    @NSManaged public func removeFromSavedColors(at indexes: NSIndexSet)

    @objc(replaceObjectInSavedColorsAtIndex:withObject:)
    @NSManaged public func replaceSavedColors(at idx: Int, with value: SavedColorGroup)

    @objc(replaceSavedColorsAtIndexes:withSavedColors:)
    @NSManaged public func replaceSavedColors(at indexes: NSIndexSet, with values: [SavedColorGroup])

    @objc(addSavedColorsObject:)
    @NSManaged public func addToSavedColors(_ value: SavedColorGroup)

    @objc(removeSavedColorsObject:)
    @NSManaged public func removeFromSavedColors(_ value: SavedColorGroup)

    @objc(addSavedColors:)
    @NSManaged public func addToSavedColors(_ values: NSOrderedSet)

    @objc(removeSavedColors:)
    @NSManaged public func removeFromSavedColors(_ values: NSOrderedSet)

}

// MARK: Generated accessors for trixySettings
extension Root {

    @objc(insertObject:inTrixySettingsAtIndex:)
    @NSManaged public func insertIntoTrixySettings(_ value: LightSettings, at idx: Int)

    @objc(removeObjectFromTrixySettingsAtIndex:)
    @NSManaged public func removeFromTrixySettings(at idx: Int)

    @objc(insertTrixySettings:atIndexes:)
    @NSManaged public func insertIntoTrixySettings(_ values: [LightSettings], at indexes: NSIndexSet)

    @objc(removeTrixySettingsAtIndexes:)
    @NSManaged public func removeFromTrixySettings(at indexes: NSIndexSet)

    @objc(replaceObjectInTrixySettingsAtIndex:withObject:)
    @NSManaged public func replaceTrixySettings(at idx: Int, with value: LightSettings)

    @objc(replaceTrixySettingsAtIndexes:withTrixySettings:)
    @NSManaged public func replaceTrixySettings(at indexes: NSIndexSet, with values: [LightSettings])

    @objc(addTrixySettingsObject:)
    @NSManaged public func addToTrixySettings(_ value: LightSettings)

    @objc(removeTrixySettingsObject:)
    @NSManaged public func removeFromTrixySettings(_ value: LightSettings)

    @objc(addTrixySettings:)
    @NSManaged public func addToTrixySettings(_ values: NSOrderedSet)

    @objc(removeTrixySettings:)
    @NSManaged public func removeFromTrixySettings(_ values: NSOrderedSet)

}
