//
//  Light+CoreDataClass.swift
//  
//
//  Created by Aardvark on 2/17/19.
//
//

import Foundation
import CoreData

@objc(Light)
public class Light: NSManagedObject {

    var peripheral: BLEPeripheral?
}
