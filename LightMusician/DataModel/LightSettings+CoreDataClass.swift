//
//  LightSettings+CoreDataClass.swift
//  
//
//  Created by Aardvark on 2/17/19.
//
//

import Foundation
import CoreData
import UIKit

var NextGroupNumber = 1
let initialColors: [UIColor] = [.red, .green, .blue, .yellow, .orange, .red, .green, .blue, .yellow, .orange]
@objc(LightSettings)
public class LightSettings: NSManagedObject {

    var lastBeatTime = Date.timeIntervalSinceReferenceDate
    var lastMaxAmplitude: Float = 0.0
    var lastSecMaxAmplitude: Float = 0.0
    var nextColorIndex = 0
    
    //assume maximum of 10 fades, and at most every .1 second
    var numFades = 10
    var fadeInterval = 0.0
    var nextFadeInterval = 0.0
    var fadeCountLeft = 0
    var isTrixy = false

    func initializeObject(_ _isTrixy: Bool = false) {
        highFrequency = 5000
        lowFrequency = 0
        isTrixy = _isTrixy
        if !_isTrixy {
            name = "Group \(NextGroupNumber)"
            NextGroupNumber += 1
            numTrixyLights = 0
        } else {
            name = "TrixyGroup"
            numTrixyLights = 10
        }
        suppression = 0.1
        threshold = 1.33
        numColors = 4
        fade = 0.5
        for index in [Int](0..<10) {
            let colorSet = ColorSet(context: self.managedObjectContext!)
            colorSet.setColor(color: initialColors[index])
            addToColors(colorSet)
        }
        lightPattern = 0
    }
    
    func calculateFadeValues() {
        let multiplier: Float = isTrixy ? 3.0 : 10.0
        numFades = Int(fade * multiplier)
        if fade > 0 && numFades == 0 {
            numFades = 1
        }
        if numFades > 0 {
            fadeInterval = Double(fade) / Double(numFades)
        } else {
            fadeInterval = 1000
        }
    }
    
    func setColors(fromColors: NSOrderedSet) {
        for index in [Int](0..<10) {
            if fromColors.count > index {
                let fromColorSet = fromColors[index] as! ColorSet
                let toColorSet = colors![index] as! ColorSet
                toColorSet.setColor(fromColorSet: fromColorSet)
            }
        }
    }
}
