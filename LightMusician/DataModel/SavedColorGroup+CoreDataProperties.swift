//
//  SavedColorGroup+CoreDataProperties.swift
//  
//
//  Created by Aardvark on 3/3/19.
//
//

import Foundation
import CoreData


extension SavedColorGroup {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SavedColorGroup> {
        return NSFetchRequest<SavedColorGroup>(entityName: "SavedColorGroup")
    }

    @NSManaged public var root: Root?
    @NSManaged public var colors: NSOrderedSet?

}

// MARK: Generated accessors for colors
extension SavedColorGroup {

    @objc(insertObject:inColorsAtIndex:)
    @NSManaged public func insertIntoColors(_ value: ColorSet, at idx: Int)

    @objc(removeObjectFromColorsAtIndex:)
    @NSManaged public func removeFromColors(at idx: Int)

    @objc(insertColors:atIndexes:)
    @NSManaged public func insertIntoColors(_ values: [ColorSet], at indexes: NSIndexSet)

    @objc(removeColorsAtIndexes:)
    @NSManaged public func removeFromColors(at indexes: NSIndexSet)

    @objc(replaceObjectInColorsAtIndex:withObject:)
    @NSManaged public func replaceColors(at idx: Int, with value: ColorSet)

    @objc(replaceColorsAtIndexes:withColors:)
    @NSManaged public func replaceColors(at indexes: NSIndexSet, with values: [ColorSet])

    @objc(addColorsObject:)
    @NSManaged public func addToColors(_ value: ColorSet)

    @objc(removeColorsObject:)
    @NSManaged public func removeFromColors(_ value: ColorSet)

    @objc(addColors:)
    @NSManaged public func addToColors(_ values: NSOrderedSet)

    @objc(removeColors:)
    @NSManaged public func removeFromColors(_ values: NSOrderedSet)

}
