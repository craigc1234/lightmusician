//
//  Root+CoreDataClass.swift
//  
//
//  Created by Aardvark on 2/17/19.
//
//

import Foundation
import CoreData

@objc(Root)
public class Root: NSManagedObject {

    var currentLightGroupIndex = 0
    
    var peripheralNameToLightMap = [String: Light]()

    var unconnectedLights = [Light]()
    var unconnectedBoxes = [Light]()
    
    func initializeObject() {
        for lightSettingObj in AppDelegate.rootDataObject!.lightSettings! {
            if let lightSettings = lightSettingObj as? LightSettings {
                lightSettings.calculateFadeValues()
                for lightObj in lightSettings.lights! {
                    if let light = lightObj as? Light {
                        peripheralNameToLightMap[light.name!] = light
                    }
                }
            }
        }
        NextGroupNumber = (AppDelegate.rootDataObject!.lightSettings?.count)! + 1
        for lightSettingObj in AppDelegate.rootDataObject!.trixySettings! {
            if let lightSettings = lightSettingObj as? LightSettings {
                for lightObj in lightSettings.lights! {
                    if let light = lightObj as? Light {
                        peripheralNameToLightMap[light.name!] = light
                    }
                }
            }
        }
    }
    
    func addGroup() {
        let lightSettings = LightSettings(context: AppDelegate.managedObjectContext!)
        lightSettings.initializeObject()
        addToLightSettings(lightSettings)
    }
    
    func addPeripheral(peripheral: BLEPeripheral)
    {
        let name = peripheral.getName()!
        if name.hasPrefix("ZT") {
            if let light = peripheralNameToLightMap[name] {
                light.peripheral = peripheral
            } else {
                let box = Light(context: AppDelegate.managedObjectContext!)
                unconnectedBoxes.append(box)
            }
        } else {
            if let light = peripheralNameToLightMap[name] {
                light.peripheral = peripheral
            } else {
                let newLight = Light(context: AppDelegate.managedObjectContext!)
                newLight.peripheral = peripheral
                newLight.name = peripheral.getName()
                
                //if new, in all cases add to first group
                if let lightSettings = AppDelegate.rootDataObject?.lightSettings![0] as? LightSettings {
                    lightSettings.addToLights(newLight)
                }
            }
        }
    }
    
    func getConnectedLightPeripherals() -> [BLEPeripheral] {
        var peripheralArray = [BLEPeripheral]()
        for (name, light) in peripheralNameToLightMap {
            if !name.hasPrefix("ZT") && light.peripheral != nil && (light.peripheral?.connected)! {
                peripheralArray.append(light.peripheral!)
            }
        }
        return peripheralArray
    }
    
    func getConnectedTrixyPeripherals() -> [BLEPeripheral] {
        var peripheralArray = [BLEPeripheral]()
        for (name, light) in peripheralNameToLightMap {
            if name.hasPrefix("ZT") && light.peripheral != nil && (light.peripheral?.connected)! {
                peripheralArray.append(light.peripheral!)
            }
        }
        return peripheralArray
    }
}
