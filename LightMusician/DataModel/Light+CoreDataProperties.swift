//
//  Light+CoreDataProperties.swift
//  
//
//  Created by Aardvark on 2/17/19.
//
//

import Foundation
import CoreData


extension Light {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Light> {
        return NSFetchRequest<Light>(entityName: "Light")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var group: LightSettings?

}
