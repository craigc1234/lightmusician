//
//  ColorViewCell.swift
//  LightMusician
//
//  Created by Aardvark on 2/5/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

class ColorViewCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: UIImageView!
}
