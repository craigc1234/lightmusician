//
//  ColorCollectionViewCell.swift
//  LightMusician
//
//  Created by Aardvark on 3/3/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

class ColorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var colorCollectionView: UICollectionView!
}
