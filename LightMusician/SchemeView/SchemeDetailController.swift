//
//  SchemeDetailController.swift
//  ColorGnome
//
//  Created by Aardvark on 12/31/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import UIKit
import ChameleonFramework
import MobileCoreServices

class SchemeDetailController: UITableViewController {
    
    @IBOutlet weak var colorPicker: HSBColorPicker!
    @IBOutlet weak var schemeSegmentedChooser: UISegmentedControl!
    @IBOutlet weak var numColorsSegmentedControl: UISegmentedControl!

    @IBOutlet weak var colorCollectionView: UICollectionView!
    var selectedColorSwatch: UIImageView?
    var selectedColorIndex = 0
    
    var currentSetting: LightSettings?
    var sourceIndexPath: IndexPath?
    var destinationIndexPath: IndexPath?
    
    static var scheme = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = Color.lightBackground.value
    }
    
    func setColor(color: UIColor) {
  
        (currentSetting!.colors![selectedColorIndex] as! ColorSet).setColor(color: color)
        let indexPath = IndexPath(item: selectedColorIndex, section: 0)
        let cell = colorCollectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = color
        saveContext()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCellID")!
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "savedColorsHeaderCellID") as! SaveColorButtonHeaderView
            cell.delegate = self
            cell.colorButton!.backgroundColor = Color.theme.value
            cell.colorButton!.layer.cornerRadius = 5
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        return (AppDelegate.rootDataObject?.savedColors?.count)!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 160
            } else if indexPath.row == 2 {
                return 50
            }
        } else {
            return 50
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "colorPickerCellID", for: indexPath) as! SchemeViewCell
                colorPicker = cell.colorPicker
                colorPicker?.delegate = self
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "numColorsCellID", for: indexPath) as! SchemeViewCell
                numColorsSegmentedControl = cell.numColorsSegmentedControl
                cell.numColorsDelegate = self
                numColorsSegmentedControl.selectedSegmentIndex = Int(currentSetting!.numColors - 2)

                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "colorsCellID", for: indexPath) as! ColorTableViewCell
                cell.tag = indexPath.row
                colorCollectionView = cell.colorCollectionView
                cell.colorCollectionView.tag = -1
                cell.colorCollectionView.dropDelegate = self
                cell.colorCollectionView.dragDelegate = self
                cell.colorCollectionView.dragInteractionEnabled = true
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "colorsCellID", for: indexPath) as! ColorTableViewCell
            cell.tag = indexPath.row
            cell.colorCollectionView.tag = indexPath.row
            cell.colorCollectionView.isUserInteractionEnabled = false
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            let currentColors = currentSetting?.colors
            let selectedColors = AppDelegate.rootDataObject?.savedColors![indexPath.row] as? SavedColorGroup
            var index = 0
            for colorObj in currentColors! {
                if let color = colorObj as? ColorSet {
                    let savedColor = selectedColors!.colors![index]
                    color.setColor(fromColorSet: savedColor as! ColorSet)
                    index += 1
                }
            }
            colorCollectionView.reloadData()
        }
        return nil
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return false
        } else {
            return true
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            AppDelegate.rootDataObject?.removeFromSavedColors(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        saveContext()
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return false
        } else {
            return false
        }
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
        if to.section == 1 {
            saveContext()
        }
    }
}

extension SchemeDetailController: SaveColorsDelegate {
    
    func saveColors() {
        let colorsToSave = currentSetting?.colors
        let newGroup = SavedColorGroup(context: (AppDelegate.rootDataObject?.managedObjectContext)!)
        AppDelegate.rootDataObject?.addToSavedColors(newGroup)
        for colorObj in colorsToSave! {
            if let color = colorObj as? ColorSet {
                let newColorSet = ColorSet(context: (AppDelegate.rootDataObject?.managedObjectContext)!)
                newColorSet.setColor(fromColorSet: color)
                newGroup.addToColors(newColorSet)
            }
        }
        tableView.reloadData()
    }
}

extension SchemeDetailController: HSBColorPickerDelegate {
    
    func HSBColorColorPickerTouched(sender:HSBColorPicker, color:UIColor, point:CGPoint, state:UIGestureRecognizer.State)
    {
        setColor(color: color)
    }
}

extension SchemeDetailController: NumColorsDelegate {
    func numColorsChanged(newNumber: Int) {
        currentSetting!.numColors = Int16(newNumber)
        colorCollectionView.reloadData()
        let count = AppDelegate.rootDataObject?.savedColors?.count
        for index in (0..<count!) {
            let indexPath = IndexPath(item: index, section: 1)
            if let cell = tableView.cellForRow(at: indexPath) as? ColorTableViewCell {
                cell.colorCollectionView.reloadData()
            }
        }
        saveContext()
    }
}

extension SchemeDetailController: UICollectionViewDelegate, UICollectionViewDataSource,
                UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        var numItems: CGFloat = 10.0
        numItems = CGFloat(currentSetting!.numColors)
        var width = (collectionView.bounds.width - (2.0 * (numItems - 1.0))) / numItems
        var height = collectionView.bounds.height - 2
        
        width = min(width, 40)
        height = max(height, 0)
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Int(currentSetting!.numColors)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorViewCellID", for: indexPath)
        
        // Configure the cell
        cell.layer.borderWidth = 3.0
        cell.layer.borderColor = UIColor.black.cgColor
        let row = collectionView.tag
        if row == -1 {
            let color = currentSetting!.colors![indexPath.item] as! ColorSet
            cell.backgroundColor = UIColor(red: CGFloat(color.red)/255.0, green: CGFloat(color.green)/255.0, blue: CGFloat(color.blue)/255.0, alpha: 1.0)
            if indexPath.item == selectedColorIndex {
                cell.layer.borderColor = UIColor.orange.cgColor
            }
        } else {
            let colorGroup = AppDelegate.rootDataObject?.savedColors![row] as! SavedColorGroup
            let color = colorGroup.colors![indexPath.item] as! ColorSet
            cell.backgroundColor = UIColor(red: CGFloat(color.red)/255.0, green: CGFloat(color.green)/255.0, blue: CGFloat(color.blue)/255.0, alpha: 1.0)
        }
        cell.layer.cornerRadius = 3
        
        return cell
    }
    
    // Uncomment this method to specify if the specified item should be selected
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if collectionView.tag == -1 {
            selectedColorIndex = indexPath.item
            return true
        }
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.reloadData()
    }
}

extension SchemeDetailController: UICollectionViewDropDelegate, UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        return dragItems(for: indexPath)
    }
    
    func dragItems(for indexPath: IndexPath) -> [UIDragItem] {
        sourceIndexPath = indexPath
        let data = "\(indexPath.item)"
        let itemProvider = NSItemProvider(object: data as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = data
        return [dragItem]
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        self.destinationIndexPath = destinationIndexPath
        return UICollectionViewDropProposal(operation: .move, intent: .insertIntoDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {

        switch coordinator.proposal.operation {
        case .move:
            let colorSet = currentSetting?.colors![sourceIndexPath!.item] as! ColorSet
            currentSetting?.removeFromColors(at: sourceIndexPath!.item)
            currentSetting?.insertIntoColors(colorSet, at: destinationIndexPath!.item)
            collectionView.reloadData()
        default:
            return
        }
    }
}


