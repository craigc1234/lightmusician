//
//  SchemeViewCell.swift
//  LightMusician
//
//  Created by Aardvark on 3/4/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

protocol NumColorsDelegate {
    func numColorsChanged(newNumber: Int)
}

class SchemeViewCell: UITableViewCell {
    
    var numColorsDelegate: NumColorsDelegate?
    @IBOutlet weak var colorPicker: HSBColorPicker!
    @IBOutlet weak var numColorsSegmentedControl: UISegmentedControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func numColorsChanged(_ sender: Any) {
        numColorsDelegate?.numColorsChanged(newNumber: numColorsSegmentedControl.selectedSegmentIndex + 2)
    }
}
