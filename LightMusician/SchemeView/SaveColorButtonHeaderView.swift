//
//  SaveColorButtonHeaderView.swift
//  LightMusician
//
//  Created by Aardvark on 3/3/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

protocol SaveColorsDelegate {
    func saveColors()
}

class SaveColorButtonHeaderView: UITableViewCell {

    @IBOutlet weak var colorButton: UIButton?
    var delegate: SaveColorsDelegate?
    
    @IBAction func saveCurrentColors(_ sender: Any) {
        delegate?.saveColors()
    }
}
