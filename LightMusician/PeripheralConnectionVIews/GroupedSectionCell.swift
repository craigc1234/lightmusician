//
//  GroupedSectionCell.swift
//  LightMusician
//
//  Created by Aardvark on 2/19/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

protocol GroupHandler {
    func deleteGroup(section: Int)
}

class GroupedSectionCell: UITableViewCell {

    var groupHandler: GroupHandler?
    @IBOutlet weak var groupName: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        deleteButton.layer.cornerRadius = 5.0
        deleteButton.backgroundColor = .red
        deleteButton.setTitleColor(UIColor.white, for: .normal)
        
        deleteButton.isHidden = true
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonTouched(_ sender: Any) {
        if let button = sender as? UIButton {
            groupHandler?.deleteGroup(section: button.tag)
        }
    }
}
