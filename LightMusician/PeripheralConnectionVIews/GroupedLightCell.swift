//
//  GroupedLightCell.swift
//  LightMusician
//
//  Created by Aardvark on 2/19/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

protocol LampCellHandler {
    func flashButtonTouched(button: UIButton)
}
class GroupedLightCell: UITableViewCell {

    var lampCellHandler: LampCellHandler?
    
    @IBOutlet weak var lampNameText: UILabel!
    @IBOutlet weak var lampFlashButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func flashButtonTouched(_ button: UIButton) {
        lampCellHandler?.flashButtonTouched(button: button)
    }
}
