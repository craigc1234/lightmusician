//
//  SensorTableViewController.swift
//  DanceParty
//
//  Created by Craig Conner on 6/6/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import UIKit
import CoreBluetooth

class TrixyConnectionViewController: UITableViewController {

    @IBOutlet weak var numLightsPicker: UIPickerView!
    @IBOutlet weak var peripheralName: UILabel!
    
    var currentSettings: NSOrderedSet?
    var currentSettingIndex = 0
    var currentPatternIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = Color.darkBackground.value
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let lightSettings = currentSettings![0] as? LightSettings {
            let currentNumLights = Int(lightSettings.numTrixyLights)
            numLightsPicker.selectRow(currentNumLights - 1, inComponent: 0, animated: false)
            if (lightSettings.lights?.count)! > 0 {
                if let light = lightSettings.lights![0] as? Light {
                    peripheralName.text = light.name
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
}

extension TrixyConnectionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let lightSettings = currentSettings![0] as? LightSettings {
            lightSettings.numTrixyLights = Int16(row + 1)
            saveContext()
        }
    }
}
