//
//  UngroupedLightCell.swift
//  LightMusician
//
//  Created by Aardvark on 2/18/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

class UngroupedLightCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    var showSelected = false
    func initializeObject(name: String) {
        nameLabel.text = name
        nameLabel.textColor = Color.theme.value
        self.layer.borderColor = Color.theme.value.cgColor
        self.layer.borderWidth = 3.0
        self.layer.cornerRadius = 5.0
    }
    
    func setSelected(select: Bool) {
        showSelected = select
        if select {
            nameLabel.textColor = UIColor.white
            self.backgroundColor = Color.theme.value
        } else  {
            nameLabel.textColor = Color.theme.value
            self.backgroundColor = UIColor.white
        }
    }
}
