//
//  ConnectViewController.swift
//  LightMusician
//
//  Created by Aardvark on 2/15/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

class LightConnectionViewController: UIViewController, ConnectionDialogDelegate {

    @IBOutlet var groupedLampsTableView: UITableView?
    @IBOutlet var ungroupedLampsCollectionView: UICollectionView?
    
    @IBOutlet weak var ungroupedLampsLabel: UILabel!
    @IBOutlet weak var groupedLampsLabel: UILabel!
    var ungroupedLampArray = [BLEPeripheral]()
    var selectedLampArray = [BLEPeripheral]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self,
                selector: #selector(foundPeripheral),
                name: kPeripheralFoundNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ungroupedLampsLabel.backgroundColor = Color.lightBackground.value
        groupedLampsLabel.backgroundColor = Color.lightBackground.value
        
        ungroupedLampArray = [BLEPeripheral]()
        for peripheral in BLEManager.bleManager!.peripheralArray {
            let prefix = BLEManager.peripheralNamePrefix[0]
            if let name = peripheral.getName() {
                if !name.hasPrefix(prefix) && !peripheral.connected {
                    ungroupedLampArray.append(peripheral)
                }
            }
        }
    }
    

    override func viewDidAppear(_ animated: Bool) {
        BLEPeripheralInterface.peripheralInterface!.dialogDelegate = self
    }
    
    @objc func foundPeripheral(notification: Notification) {
        ungroupedLampsCollectionView?.reloadData()
        
        ungroupedLampArray = [BLEPeripheral]()
        for peripheral in BLEManager.bleManager!.peripheralArray {
            let prefix = BLEManager.peripheralNamePrefix[0]
            if let name = peripheral.getName() {
                if !name.hasPrefix(prefix) {
                    ungroupedLampArray.append(peripheral)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func showConnectingDialogs(hasBluetooth: Bool) {
        if hasBluetooth {
            showConnectingDialog()
        }
    }
    
    func dialogDismissed() {
        
    }

    @IBAction func rescanButtonTouched(_ sender: Any) {
        BLEPeripheralInterface.peripheralInterface?.doConnection()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LightConnectionViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ungroupedLampArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ungroupedLightCellID", for: indexPath) as? UngroupedLightCell
        cell?.showSelected = false
        if let name = ungroupedLampArray[indexPath.item].getName() {
            cell?.initializeObject(name: name)
        }

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? UngroupedLightCell
        let peripheral = ungroupedLampArray[indexPath.item]
        var currentlySelected = false
        var index = 0
        for selection in selectedLampArray {
            if selection == peripheral {
                selectedLampArray.remove(at: index)
                currentlySelected = true
                break
            }
            index += 1
        }
        
        if !currentlySelected {
            selectedLampArray.append(peripheral)
        }
        
        cell?.setSelected(select: !currentlySelected)
        
        groupedLampsTableView?.reloadData()
    }
}

extension LightConnectionViewController: UITableViewDataSource, UITableViewDelegate, GroupHandler {

    @IBAction func editButtonTouched(_ sender: UIButton) {
        groupedLampsTableView?.isEditing = !groupedLampsTableView!.isEditing
        
        sender.isSelected = groupedLampsTableView!.isEditing
        
        groupedLampsTableView?.reloadData()
    }
    
    func addLampsToGroup(section: Int) {
        let managedObjectContext = (AppDelegate.rootDataObject!.managedObjectContext)!
        if section >= (AppDelegate.rootDataObject?.lightSettings?.count)! {
            let newGroup = LightSettings(context: managedObjectContext)
            newGroup.initializeObject()
            AppDelegate.rootDataObject?.addToLightSettings(newGroup)
        }
        if let group = AppDelegate.rootDataObject?.lightSettings![section] as? LightSettings {
            for lamp in selectedLampArray {
                let light = Light(context: managedObjectContext)
                light.name = lamp.getName()
                light.group = group
                group.addToLights(light)
            }
        }
        var index = 0
        var removeArray = [Int]()
        for lamp in ungroupedLampArray {
            if selectedLampArray.contains(lamp) {
                removeArray.append(index)
            }
            index += 1
        }
        for index in removeArray.reversed() {
            ungroupedLampArray.remove(at: index)
        }
        selectedLampArray.removeAll()
        ungroupedLampsCollectionView?.reloadData()
        groupedLampsTableView?.reloadData()
    }
    
    func deleteGroup(section: Int) {
        AppDelegate.rootDataObject?.removeFromLightSettings(at: section)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var addNewSection = 0
        if selectedLampArray.count > 0 {
            addNewSection = 1
        }
        
        return (AppDelegate.rootDataObject?.lightSettings?.count)! + addNewSection
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lightGroupSectionCellID") as? GroupedSectionCell
        cell?.groupHandler = self
        cell?.tag = section
        if section < (AppDelegate.rootDataObject?.lightSettings!.count)! {
            cell?.groupName.text = (AppDelegate.rootDataObject?.lightSettings![section] as! LightSettings).name
        } else {
            cell?.groupName.text = "Group \(NextGroupNumber)"
            cell?.deleteButton.isHidden = true
        }
        if tableView.isEditing {
            cell?.deleteButton.isHidden = false
            cell?.addButton.isHidden = true
        } else {
            cell?.deleteButton.isHidden = true
            cell?.addButton.isHidden = true
            if selectedLampArray.count > 0 {
                cell?.addButton.isHidden = false
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section >= AppDelegate.rootDataObject!.lightSettings!.count {
            return 1
        }
        let section = AppDelegate.rootDataObject!.lightSettings![section] as! LightSettings
        var count = section.lights?.count
        if count == 0 {
            count = 1
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section >= AppDelegate.rootDataObject!.lightSettings!.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "noLampsCellID", for: indexPath)
            return cell
        }
        let section = AppDelegate.rootDataObject!.lightSettings![indexPath.section] as! LightSettings
        let count = section.lights?.count
        if count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "noLampsCellID", for: indexPath)
            return cell
        }
        let lamp = section.lights![indexPath.row] as! Light
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupedLightCell", for: indexPath) as! GroupedLightCell
        cell.lampNameText.text = lamp.name
        return cell
    }
}
