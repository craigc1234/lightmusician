//
//  GroupedHeaderView.swift
//  LightMusician
//
//  Created by Aardvark on 2/28/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit

class GroupedHeaderView: UITableViewHeaderFooterView {

    var groupHandler: GroupHandler?
    @IBOutlet weak var groupName: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        deleteButton.layer.cornerRadius = 5.0
        deleteButton.backgroundColor = .red
        deleteButton.setTitleColor(UIColor.white, for: .normal)
        
        deleteButton.isHidden = true
    }
    
    @IBAction func deleteButtonTouched(_ sender: Any) {
        if let button = sender as? UIButton {
            groupHandler?.deleteGroup(section: button.tag)
        }
    }

}
