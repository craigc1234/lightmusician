//
//  LightGroupController.swift
//  LightMusician
//
//  Created by Aardvark on 2/22/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import UIKit
import MobileCoreServices

class LightGroupController: UITableViewController, GroupHandler, LampCellHandler,
UITableViewDragDelegate, UITableViewDropDelegate {

    var editButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "GroupedHeaderView", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "lightHeaderID")
        
        tableView.dragDelegate = self
        tableView.dropDelegate = self
        tableView.dragInteractionEnabled = true
        tableView.backgroundColor = Color.lightBackground.value
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        var lightGroupCount = 0
        if let lightGroups = AppDelegate.rootDataObject?.lightSettings {
            lightGroupCount = lightGroups.count
        }
        return lightGroupCount
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "lightHeaderID") as? GroupedHeaderView
        let lightSetting = (AppDelegate.rootDataObject?.lightSettings![section])! as! LightSettings
        headerCell?.groupName.text = lightSetting.name
        headerCell?.groupHandler = self
        if tableView.isEditing {
            headerCell?.deleteButton.isHidden = false
            headerCell?.deleteButton.tag = section
        }
        headerCell!.backgroundColor = Color.lightBackground.value

        return headerCell
    }

    
    @IBAction func editTable(_ editButton: UIButton) {
        self.tableView.isEditing = !self.tableView.isEditing
        if self.tableView.isEditing {
            editButton.setTitle("Done", for: .normal)
        } else {
            editButton.setTitle("Edit", for: .normal)
        }
        tableView.reloadData()
    }

    @IBAction func addGroupButtonTouched(_ sender: Any) {
        AppDelegate.rootDataObject?.addGroup()
        tableView.reloadData()
    }
    
    func deleteGroup(section: Int) {
        AppDelegate.rootDataObject?.removeFromLightSettings(at: section)
        if AppDelegate.rootDataObject?.lightSettings?.count == 0 {
            NextGroupNumber = 1
            addGroupButtonTouched(editButton as Any)
        }
        tableView.reloadData()
    }
    
    func flashButtonTouched(button: UIButton) {
        let lampNumber = button.tag % 10
        let groupNumber = Int(Float(button.tag) / 10.0)
        
        let lightSetting = (AppDelegate.rootDataObject?.lightSettings![groupNumber])! as! LightSettings
        let light = lightSetting.lights![lampNumber] as! Light
        if let peripheral = light.peripheral {
            LightControl.lightControl.flashPeripheral(peripheral)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if let lightGroups = AppDelegate.rootDataObject?.lightSettings {
            if section == lightGroups.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCellID") as? GroupedLightCell
                cell?.backgroundColor = Color.lightBackground.value
                cell?.lampNameText.text = ""
                cell?.lampFlashButton.isHidden = true
                return cell
            }
        }
        return nil
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let lightSetting = (AppDelegate.rootDataObject?.lightSettings![section])! as! LightSettings
        return lightSetting.lights!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nameCellID", for: indexPath) as? GroupedLightCell

        let lightSetting = (AppDelegate.rootDataObject?.lightSettings![indexPath.section])! as! LightSettings
        let light = lightSetting.lights![indexPath.row] as! Light
        cell?.lampNameText.text = light.name
        cell?.lampFlashButton.tag = indexPath.row + indexPath.section * 10
        cell?.lampCellHandler = self
        if light.peripheral != nil && (light.peripheral?.connected)! {
            cell?.lampFlashButton.tintColor = .green
        } else {
            cell?.lampFlashButton.tintColor = .red
        }

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let light: Light
        if editingStyle == .delete {
            let fromLightSetting = AppDelegate.rootDataObject?.lightSettings![indexPath.section] as! LightSettings
            light = fromLightSetting.lights![indexPath.row] as! Light
            fromLightSetting.removeFromLights(at: indexPath.row)
            
            if light.peripheral != nil && (light.peripheral?.connected)! {
                BLEManager.bleManager?.disconnectFromPeripheral(blePeripheral: light.peripheral!)
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        saveContext()
    }

    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
        let light: Light
        let fromLightSetting = AppDelegate.rootDataObject?.lightSettings![fromIndexPath.section] as! LightSettings
        light = fromLightSetting.lights![fromIndexPath.row] as! Light
        fromLightSetting.removeFromLights(at: fromIndexPath.row)
        
        let toLightSetting = AppDelegate.rootDataObject?.lightSettings![to.section] as! LightSettings
        toLightSetting.insertIntoLights(light, at: to.row)

        saveContext()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        return dragItems(for: indexPath)
    }
    func dragItems(for indexPath: IndexPath) -> [UIDragItem] {
        let placeName = "row"
        
        let data = placeName.data(using: .utf8)
        let itemProvider = NSItemProvider()
        
        itemProvider.registerDataRepresentation(forTypeIdentifier: kUTTypePlainText as String, visibility: .all) { completion in
            completion(data, nil)
            return nil
        }
        
        return [
            UIDragItem(itemProvider: itemProvider)
        ]
    }
    
    func tableView(_ tableView: UITableView, canHandle session: UIDropSession) -> Bool {
        return canHandle(session)
    }

    func canHandle(_ session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSString.self)
    }

    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {
        // The .move operation is available only for dragging within a single app.
        if tableView.hasActiveDrag {
            if session.items.count > 1 {
                return UITableViewDropProposal(operation: .cancel)
            } else {
                return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
            }
        } else {
            return UITableViewDropProposal(operation: .copy, intent: .insertAtDestinationIndexPath)
        }
    }

    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        let destinationIndexPath: IndexPath
        
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            // Get last index path of table view.
            let section = tableView.numberOfSections - 1
            let row = tableView.numberOfRows(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
        }
        
        coordinator.session.loadObjects(ofClass: NSString.self) { items in
            // Consume drag items.
            let stringItems = items as! [String]
            
            var indexPaths = [IndexPath]()
            for (index, item) in stringItems.enumerated() {
                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)
              //  self.model.addItem(item, at: indexPath.row)
                indexPaths.append(indexPath)
            }
            
            tableView.insertRows(at: indexPaths, with: .automatic)
        }
    }
}
