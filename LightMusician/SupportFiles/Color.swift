//
//  Color.swift
//  TeamSudoku
//
//  Created by Craig Conner on 8/25/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//  Borrowed from https://medium.com/@sauvik_dolui/
    //a-smart-way-to-manage-colours-schemes-for-ios-applications-development-923ef976be55

//  Chameleon documented here https://github.com/ViccAlexander/Chameleon
// swiftlint:disable identifier_name

import UIKit
typealias ColorType = UIColor

import ChameleonFramework

let THEME_COLOR = UIColor(red: 255/255, green: 120/255, blue: 0/255, alpha: 1.0)
let LIGHT_BACKGROUND_COLOR = UIColor(red: 247/255, green: 233/255, blue: 218/255, alpha: 1.0)
let DARK_BACKGROUND_COLOR = UIColor(red: 255/255, green: 212/255, blue: 164/255, alpha: 1.0)
let BORDER_COLOR = ColorType.white
let LIGHT_TEXT_COLOR = ColorType.flatWhite()!
let DARKER_BACKGROUND_COLOR = UIColor(red: 121/255, green: 135/255, blue: 160/255, alpha: 1.0)
//UIColor(red: 129/255, green: 153/255, blue: 204/255, alpha: 1.0)
let SHADOW_COLOR = UIColor(red: 87/255, green: 145/255, blue: 255/255, alpha: 1.0)
let DARK_SHADOW_COLOR = ColorType.flatSandColorDark()!
let DISABLED_COLOR = ColorType.flatGray()!

enum Color {

    case theme
    case border
    case shadow
    case darkShadow
    case disabled
    case darkBackground
    case darkerBackground

    case darkBackgroundGradient

    case lightBackground
    case intermediateBackground

    case darkText
    case lightText
    case intermediateText

    case affirmation
    case negation
    // 1
    case custom(hexString: String, alpha: Double)
    // 2
    func withAlpha(_ alpha: Double) -> ColorType {
        return self.value.withAlphaComponent(CGFloat(alpha))
    }
}

extension Color {

    var value: ColorType {
        var instanceColor = ColorType.clear

        switch self {
        case .border:
            instanceColor = BORDER_COLOR
        case .theme:
            instanceColor = THEME_COLOR
        case .shadow:
            instanceColor = SHADOW_COLOR
        case .disabled:
            instanceColor = DISABLED_COLOR
        case .darkShadow:
            instanceColor = DARK_SHADOW_COLOR
        case .darkBackground:
            instanceColor = DARK_BACKGROUND_COLOR
        case .darkerBackground:
            instanceColor = DARKER_BACKGROUND_COLOR
        case .darkBackgroundGradient:
            break
        //    instanceColor = GradientColor(gradientStyle: .leftToRight, frame: UIScreen.main.bounds,
       //     colors: [ColorType.flatPowderBlue(),
       //     ColorType.flatPowderBlueColorDark()])
        case .lightBackground:
            instanceColor = LIGHT_BACKGROUND_COLOR
        case .intermediateBackground:
            instanceColor = ColorType(hexString: "#cccc99")
        case .darkText:
            instanceColor = ColorType(hexString: "#333333")
        case .intermediateText:
            instanceColor = ColorType(hexString: "#999999")
        case .lightText:
            instanceColor = LIGHT_TEXT_COLOR
        case .affirmation:
            instanceColor = ColorType(hexString: "#008B66") //92cad4 a9d492
        case .negation:
            instanceColor = ColorType(hexString: "#92cad4")
        case .custom(let hexValue, let opacity):
            instanceColor = ColorType(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
        }
        return instanceColor
    }
}

extension ColorType {

    /**
     Creates an ColorType from HEX String in "#363636" format
     
     - parameter hexString: HEX String in "#363636" format
     - returns: ColorType from HexString
     */
    @objc convenience init(hexString: String) {

        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner          = Scanner(string: hexString as String)

        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }

    /**
     Creates an ColorType Object based on provided RGB value in integer
     - parameter red:   Red Value in integer (0-255)
     - parameter green: Green Value in integer (0-255)
     - parameter blue:  Blue Value in integer (0-255)
     - returns: ColorType with specified RGB values
     */
    @objc convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}

extension UIColor {
    static func IndexesToViewColor(_ color: [Int]) -> UIColor {
        var alpha: CGFloat = 1
        var colorsInMix = color[0] > 0 ? 1 : 0
        colorsInMix +=  color[1] > 0 ? 1 : 0
        colorsInMix += color[2] > 0 ? 1 : 0
        colorsInMix += color[3] > 0 ? 1 : 0

        if colorsInMix != 0 {
            alpha = CGFloat(1.0 - (Double(color[3])/255.0)/Double(colorsInMix))
        }
        print(color)
        let red = CGFloat(color[0])/255
        let green = CGFloat(color[1])/255
        let blue = CGFloat(color[2])/255
        return UIColor.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    static func IndexesToHSBColor(_ color: [Int]) -> UIColor {
        let alpha: CGFloat = 1
        var colorsInMix = color[0] > 0 ? 1 : 0
        colorsInMix +=  color[1] > 0 ? 1 : 0
        colorsInMix += color[2] > 0 ? 1 : 0

        let red = CGFloat(color[0])/255
        let green = CGFloat(color[1])/255
        let blue = CGFloat(color[2])/255
        return UIColor.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    func toRGB() -> [Int] {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha)

        return [max(Int(fRed*255.0), 0), max(Int(fGreen*255.0), 0), max(Int(fBlue*255.0), 0), 0]
    }
}
