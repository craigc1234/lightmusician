//
//  SpectralView.swift
//  TempiHarness
//
//  Created by John Scalo on 1/20/16.
//  Copyright © 2016 John Scalo. All rights reserved.
//

import UIKit

struct AmplitudeInfo {
    let magnitude: Float
    let couldBeBeat: Bool
    let notSmoothedBeat: Bool
    let isBeat: Bool
    var isNegativeBeat = false
    init(newMagnitude: Float, markBeat: Bool, markedBeat: Bool, nonSmoothedBeat: Bool) {
        magnitude = newMagnitude
        isBeat = markBeat
        couldBeBeat = markedBeat
        self.notSmoothedBeat = nonSmoothedBeat
    }
}

class AmplitudeView: UIView {

    var minMagnitude:Float = 0.1
    var magnitudes = [AmplitudeInfo]()
    var pixelWidth = 1
    var viewHeight: CGFloat = 0.0
    var averageHeight: CGFloat = 0.0
    var averageHeightPosition: CGFloat = 0.0
    var basePosition: CGFloat = 0.0
    var average: Float = 0.0
    var viewStartPos: CGFloat = 1.0
    var viewWidth: CGFloat = 1.0

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setScale() {
        let screenScale = UIScreen.main.scale
        pixelWidth = 300 //Int(screenScale * self.bounds.width * 0.4)
        viewStartPos = 20
        viewHeight = self.bounds.height
        basePosition = viewHeight * 0.2
        viewHeight = viewHeight * 0.7
        averageHeight = viewHeight * 0.25
        averageHeightPosition = averageHeight + basePosition
    }
    
    func addMagnitude(newMag: Float, newAverage: Float, isBeat: Bool, couldBeBeat: Bool, nonSmoothedBeat: Bool, isNegativeBeat: Bool) {
        
        average = newAverage
        let magHeightPixels = min(averageHeight * CGFloat(newMag / newAverage), viewHeight)
        
        if isBeat {
         //   print(magHeightPixels, magnitudes.last?.magnitude)
        }
        var aI = AmplitudeInfo(newMagnitude: Float(magHeightPixels), markBeat: isBeat, markedBeat: couldBeBeat, nonSmoothedBeat: nonSmoothedBeat)
        aI.isNegativeBeat = isNegativeBeat
        magnitudes.append(aI)
        if magnitudes.count > pixelWidth {
            magnitudes.removeFirst(magnitudes.count - pixelWidth)
        }
    }
    override func draw(_ rect: CGRect) {
        
        let context = UIGraphicsGetCurrentContext()
        
        
        
        self.drawAmplitudeRun(context: context!)
    }
    
    //starting with time zero = left bounds, draw out pixel width number of amplitude
    //magnitudes
    private func drawAmplitudeRun(context: CGContext) {
        let viewWidth = self.bounds.size.width
        let viewHeight = self.bounds.size.height
        
        let colWidth = tempi_round_device_scale(d: (viewWidth - (2 * viewStartPos)) / CGFloat(pixelWidth))
        
        context.saveGState()
        context.scaleBy(x: 1, y: -1)
        context.translateBy(x: 0, y: -viewHeight)
        
        let colors = [UIColor.yellow.cgColor, UIColor.red.cgColor]
        let gradient = CGGradient(
            colorsSpace: nil, // generic color space
            colors: colors as CFArray,
            locations: nil)
        
        let beatColors = [UIColor.green.cgColor, UIColor.green.cgColor]
        let beatGradient = CGGradient(
            colorsSpace: nil, // generic color space
            colors: beatColors as CFArray,
            locations: nil)
        
        let negativeBeatColors = [UIColor.blue.cgColor, UIColor.blue.cgColor]
        let negativeBeatGradient = CGGradient(
            colorsSpace: nil, // generic color space
            colors: negativeBeatColors as CFArray,
            locations: nil)
        
        var x: CGFloat = viewStartPos

        // Draw the spectrum.
        for aI in magnitudes {
            let magnitude = aI.magnitude
            if magnitude > minMagnitude {

                var beatBottomPos = basePosition
                var magHeight = CGFloat(magnitude) + basePosition
                if aI.notSmoothedBeat {
                    beatBottomPos = beatBottomPos / 3
                    magHeight += beatBottomPos * 2
                } else if aI.couldBeBeat {
                    beatBottomPos = 2 * beatBottomPos / 3
                    magHeight += beatBottomPos
                }
                let colRect: CGRect = CGRect(x: x, y: beatBottomPos, width: colWidth, height: magHeight)
                
                let startPoint = CGPoint(x: viewWidth / 2, y: 0)
                let endPoint = CGPoint(x: viewWidth / 2, y: viewHeight)
                
                context.saveGState()
                context.clip(to: colRect)
                if aI.isBeat {
                    context.drawLinearGradient(beatGradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
                } else if aI.isNegativeBeat {
                    context.drawLinearGradient(negativeBeatGradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
                } else {
                    context.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
                }
                context.restoreGState()
            }
            x += colWidth
        }
        context.restoreGState()
    }
}
