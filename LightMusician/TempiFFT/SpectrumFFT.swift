//
//  TempiFFT.swift
//  TempiBeatDetection
//
//  Created by John Scalo on 1/12/16.
//  Copyright © 2016 John Scalo. See accompanying License.txt for terms.

/*  A functional FFT built atop Apple's Accelerate framework for optimum performance on any device. In addition to simply performing the FFT and providing access to the resulting data, TempiFFT provides the ability to map the FFT spectrum data into logical bands, either linear or logarithmic, for further analysis.

E.g.

let fft = TempiFFT(withSize: frameSize, sampleRate: 44100)

// Setting a window type reduces errors
fft.windowType = TempiFFTWindowType.hanning

// Perform the FFT
fft.fftForward(samples)

// Map FFT data to logical bands. This gives 4 bands per octave across 7 octaves = 28 bands.
fft.calculateLogarithmicBands(minFrequency: 100, maxFrequency: 11025, bandsPerOctave: 4)

// Process some data
for i in 0..<fft.numberOfBands {
let f = fft.frequencyAtBand(i)
let m = fft.magnitudeAtBand(i)
}

Note that TempiFFT expects a mono signal (i.e. numChannels == 1) which is ideal for performance.
*/


import Foundation
import Accelerate

@objc enum TempiFFTWindowType: NSInteger {
    case none
    case hanning
    case hamming
}

class BandMagnitudes {
    var bandMagnitudes: [[Float]]!
    var bandMagnitudeTotals: [Float]!
    var numBands = 0
    var currentBandSetIndex = 0
    var nextBandSetIndex = 0
    var totalSamples = 40
    var lastBeatIndex = 0
    
    
    init(_numBands: Int) {
        numBands = _numBands
        bandMagnitudes = [[Float]](repeating: [Float](repeating: 0.0, count: numBands), count: totalSamples)
        bandMagnitudeTotals = [Float](repeating: 0.0, count: numBands)
    }
    
    func addBands(bands: [Float]) {
        var currentBandIndex = 0
        for value in bands {
            bandMagnitudeTotals[currentBandIndex] -= bandMagnitudes[nextBandSetIndex][currentBandIndex]
            bandMagnitudeTotals[currentBandIndex] += value
            bandMagnitudes[nextBandSetIndex][currentBandIndex] = value
            currentBandIndex += 1
        }
        currentBandSetIndex = nextBandSetIndex
        self.nextBandSetIndex += 1
        if nextBandSetIndex == totalSamples {
            nextBandSetIndex = 0
        }
        if nextBandSetIndex == 0 {
 //           print(bandMagnitudes)
        }
    }
    
    func printMagnitudes() {
        print(Date.timeIntervalSinceReferenceDate - SpectrumFFT.startTime, lastBeatIndex)
    //    print(bandMagnitudes)
    }
    
    func getBandAverage(band: Int) -> Float {
        return bandMagnitudeTotals[band] / Float(totalSamples)
    }
    
    func calulateBeat(threshold: Float) -> Bool {
        let newThreshold = threshold * 10
        var currentBand = bandMagnitudes[currentBandSetIndex]
        lastBeatIndex = 0
        for magValueTotalForBand in bandMagnitudeTotals {
            let ave = magValueTotalForBand / Float(totalSamples)
            let currentValue = currentBand[lastBeatIndex]
            if currentValue > 1.0 && currentValue > ave * newThreshold {
               print(currentValue, ave, newThreshold, lastBeatIndex)
                return true
            }
            lastBeatIndex += 1
        }
        
        return false
    }
    
    func resetSavedValues() {
        nextBandSetIndex = 0
        currentBandSetIndex = 0
    }
}

class Magnitudes {
    var magnitudes: [Float]!
    var magAve: Float = 0
    var totalSamples = 100
    var numSavedSamples = 0
    var nextMagIndex = 0
    
    init() {
        magnitudes = [Float](repeating: 0.0, count: totalSamples)
    }
    
    func addMagnitude(newMag: Float) {
        magAve -= magnitudes[nextMagIndex]
        magAve += newMag
        magnitudes[nextMagIndex] = newMag
        print(newMag, magAve, numSavedSamples, nextMagIndex)
        self.nextMagIndex += 1
        if nextMagIndex == totalSamples {
            nextMagIndex = 0
        }
        if numSavedSamples < totalSamples {
            numSavedSamples += 1
        }
    }
    
    func resetSavedValues() {
        numSavedSamples = 0
        nextMagIndex = 0
        magAve = 100
    }
    
    func getMagAverage() -> Float {
        return magAve / Float(totalSamples)
    }
}

@objc class SpectrumFFT : NSObject {
    
    static let startTime = Date.timeIntervalSinceReferenceDate
    static var numBandsPerOctave = 6
    static var numOctaves = 6

    /// The length of the sample buffer we'll be analyzing.
    private(set) var size: Int
    
    /// The sample rate provided at init time.
    private(set) var sampleRate: Float
    
    /// The Nyquist frequency is ```sampleRate``` / 2
    var nyquistFrequency: Float {
        get {
            return sampleRate / 2.0
        }
    }
    
    // After performing the FFT, contains size/2 magnitudes, one for each frequency band.
    var magnitudes: [Float]!
    
    /// After calling calculateLinearBands() or calculateLogarithmicBands(), contains a magnitude for each band.
    private(set) var bandMagnitudes: [Float]!
    static private(set) var lastBandMagnitudes: [Float]!

    /// After calling calculateLinearBands() or calculateLogarithmicBands(), contains the average frequency for each band
    private(set) var bandFrequencies: [Float]!

    /// The average bandwidth throughout the spectrum (nyquist / magnitudes.count)
    var bandwidth: Float {
        get {
            return self.nyquistFrequency / Float(self.magnitudes.count)
        }
    }
    
    /// The number of calculated bands (must call calculateLinearBands() or calculateLogarithmicBands() first).
    private(set) var numberOfBands: Int = 0
    
    var currentMaxMagnitude:Float = 0.0
    var currentMaxFrequency:Float = 0.0
    
    static var bandMagnitudeObject = BandMagnitudes(_numBands: SpectrumFFT.numOctaves * SpectrumFFT.numBandsPerOctave)
    
    /// The minimum and maximum frequencies in the calculated band spectrum (must call calculateLinearBands() or calculateLogarithmicBands() first).
    private(set) var bandMinFreq, bandMaxFreq: Float!
    
    /// Supplying a window type (hanning or hamming) smooths the edges of the incoming waveform and reduces output errors from the FFT function (aka "spectral leakage" - ewww).
    var windowType = TempiFFTWindowType.none
    
    private var halfSize:Int
    private var window:[Float]!
    private var hasPerformedFFT: Bool = false
    private var complexBuffer: DSPSplitComplex!
    static var octaveBoundaryFreqs1 = [Float]()
    static var fftSetUp: FFTSetup?
    static func initializeOctaveFrequencyRange() {
        calculateMidPoints()
        SpectrumFFT.octaveBoundaryFreqs1 = midPointRange
    }
    
    static func initSetup(size: Int = 4096) {
        let log2n = vDSP_Length(floor(log2(Float(size))))
        SpectrumFFT.fftSetUp = vDSP_create_fftsetup(
            log2n,
            FFTRadix(kFFTRadix2))
        guard SpectrumFFT.fftSetUp != nil else {
                fatalError("Can't create FFT setup.")
        }
    }
    /// Instantiate the FFT.
    /// - Parameter withSize: The length of the sample buffer we'll be analyzing. Must be a power of 2. The resulting ```magnitudes``` are of length ```inSize/2```.
    /// - Parameter sampleRate: Sampling rate of the provided audio data.
    init(withSize inSize:Int, sampleRate inSampleRate: Float) {

        self.sampleRate = inSampleRate

        self.size = inSize
        self.halfSize = inSize / 2
        
        // create fft setup
        if SpectrumFFT.fftSetUp == nil {
            SpectrumFFT.initSetup()
        }        
    }
    
    func deinitSetup() {
        // destroy the fft setup object
        vDSP_destroy_fftsetup(SpectrumFFT.fftSetUp)
    }
    
     func getMaxAmplitude(_ inMonoBuffer:[Float])  -> Float {
        var currentMax: Float = 0.0
        for sample in inMonoBuffer {
            currentMax = max(abs(sample), currentMax)
        }
        return currentMax
    }
    
    func getSumAmplitude(_ inMonoBuffer:[Float])  -> Float {
        var total: Float = 0.0
        for sample in inMonoBuffer {
            //square to get positive differentiable values
            total += sample * sample
        }
        return total
    }
    
    func getMaxAmplitudeInRange(lowFreq: Float, highFreq: Float) -> Float {
        
        var curFreq = lowFreq
        var currentMax: Float = 0.0
        while curFreq <= highFreq {
            let mag = magnitudeAtFrequency(curFreq)
            currentMax = max(mag, currentMax)
            curFreq += self.bandwidth
        }
        
        return currentMax
    }
    
    /// Perform a forward FFT on the provided single-channel audio data. When complete, the instance can be queried for information about the analysis or the magnitudes can be accessed directly.
    /// - Parameter inMonoBuffer: Audio data in mono format
    func fftForward(_ inMonoBuffer:[Float]) {
        
        var analysisBuffer = inMonoBuffer
        
        // If we have a window, apply it now. Since 99.9% of the time the window array will be exactly the same, an optimization would be to create it once and cache it, possibly caching it by size.
        if self.windowType != .none {
            
            if self.window == nil {
                self.window = [Float](repeating: 0.0, count: size)
                
                switch self.windowType {
                case .hamming:
                    vDSP_hamm_window(&self.window![0], UInt(size), 0)
                case .hanning:
                    vDSP_hann_window(&self.window![0], UInt(size), Int32(vDSP_HANN_NORM))
                default:
                    break
                }
            }
            
            // Apply the window
            vDSP_vmul(inMonoBuffer, 1, self.window, 1, &analysisBuffer, 1, UInt(inMonoBuffer.count))
        }
        
        var n = analysisBuffer.count
        if n % 2 == 1 {
            n -= 1
        }
        
        let observed: [DSPComplex] = stride(from: 0, to: Int(n), by: 2).map {
            return DSPComplex(real: analysisBuffer[$0],
                              imag: analysisBuffer[$0.advanced(by: 1)])
        }
        
        let halfN = Int(n / 2)
        
        var forwardInputReal = [Float](repeating: 0, count: halfN)
        var forwardInputImag = [Float](repeating: 0, count: halfN)
        
        var forwardInput = DSPSplitComplex(realp: &forwardInputReal,
                                           imagp: &forwardInputImag)
        
        vDSP_ctoz(observed, 2,
                  &forwardInput, 1,
                  vDSP_Length(halfN))
        
        var forwardOutputReal = [Float](repeating: 0, count: halfN)
        var forwardOutputImag = [Float](repeating: 0, count: halfN)
        self.complexBuffer = DSPSplitComplex(realp: &forwardOutputReal,
                                            imagp: &forwardOutputImag)
        
        let log2n = vDSP_Length(floor(log2(Float(size))))
        vDSP_fft_zrop(SpectrumFFT.fftSetUp!,
                      &forwardInput, 1,
                      &self.complexBuffer, 1,
                      log2n,
                      FFTDirection(kFFTDirection_Forward))
        
        let componentFrequencies = forwardOutputImag.enumerated().filter {
            $0.element < -1
            }.map {
                return $0.offset
        }
        
 
        // Store and square (for better visualization & conversion to db) the magnitudes
        self.magnitudes = [Float](repeating: 0.0, count: self.halfSize)
        vDSP_zvmags(&(self.complexBuffer!), 1, &self.magnitudes![0], 1, UInt(self.halfSize))
        
 //       print(componentFrequencies)
        var maxMag: Float = 0.0
        var maxMagIndex = 0
        for index in componentFrequencies {
            if self.magnitudes[index] > maxMag {
                maxMag = self.magnitudes[index]
                maxMagIndex = index
            }
        }
 //       print(maxMag, maxMagIndex)
        
        
        // Prints "[1, 5, 25, 30, 75, 100, 300, 500, 512, 1023]"
  //      print("CM", componentmFrequencies)
        
        self.hasPerformedFFT = true
    }
    
    /// Applies logical banding on top of the spectrum data. The bands are spaced linearly throughout the spectrum.
    func calculateLinearBands(minFrequency: Float, maxFrequency: Float, numberOfBands: Int) {
        assert(hasPerformedFFT, "*** Perform the FFT first.")
        
        currentMaxMagnitude = 0.0
        currentMaxFrequency = 0.0
        let actualMaxFrequency = min(self.nyquistFrequency, maxFrequency)
        
        self.numberOfBands = numberOfBands
        self.bandMagnitudes = [Float](repeating: 0.0, count: numberOfBands)
        self.bandFrequencies = [Float](repeating: 0.0, count: numberOfBands)
        
        let magLowerRange = magIndexForFreq(minFrequency)
        let magUpperRange = magIndexForFreq(actualMaxFrequency)
        let ratio: Float = Float(magUpperRange - magLowerRange) / Float(numberOfBands)
        
        for i in 0..<numberOfBands {
            let magsStartIdx: Int = Int(floorf(Float(i) * ratio)) + magLowerRange
            let magsEndIdx: Int = Int(floorf(Float(i + 1) * ratio)) + magLowerRange
            var magsAvg: Float
            if magsEndIdx == magsStartIdx {
                // Can happen when numberOfBands < # of magnitudes. No need to average anything.
                magsAvg = self.magnitudes[magsStartIdx]
            } else {
                magsAvg = fastAverage(self.magnitudes, magsStartIdx, magsEndIdx)
            }
            self.bandMagnitudes[i] = magsAvg
            self.bandFrequencies[i] = self.averageFrequencyInRange(magsStartIdx, magsEndIdx)
            if magsAvg > currentMaxMagnitude {
                currentMaxMagnitude = magsAvg
                currentMaxFrequency = self.bandFrequencies[i]
            }
        }
        
        self.bandMinFreq = self.bandFrequencies[0]
        self.bandMaxFreq = self.bandFrequencies.last
    }
    
    /// Applies logical banding on top of the spectrum data. The bands are grouped by octave throughout the spectrum. Note that the actual min and max frequencies in the resulting band may be lower/higher than the minFrequency/maxFrequency because the band spectrum <i>includes</i> those frequencies but isn't necessarily bounded by them.
    func calculateLogarithmicBands(minFrequency: Float, maxFrequency: Float) {
        assert(hasPerformedFFT, "*** Perform the FFT first.")
        
        // The max can't be any higher than the nyquist
        let actualMaxFrequency = min(self.nyquistFrequency, maxFrequency)
        
        // The min can't be 0 otherwise we'll divide octaves infinitely
        let actualMinFrequency = max(1, minFrequency)
        
        // Define the octave frequencies we'll be working with. Note that in order to always include minFrequency, we'll have to set the lower boundary to the octave just below that frequency.
         var octaveBoundaryFreqs: [Float] = [Float]()
        var curFreq = actualMaxFrequency
        octaveBoundaryFreqs.append(curFreq)
        repeat {
            curFreq /= 2
            octaveBoundaryFreqs.append(curFreq)
        } while curFreq > actualMinFrequency
        
        octaveBoundaryFreqs = octaveBoundaryFreqs.reversed()
        self.bandMagnitudes = [Float]()
        self.bandFrequencies = [Float]()
        
        // Break up the spectrum by octave
        for i in 0..<octaveBoundaryFreqs.count - 1 {
            let lowerFreq = octaveBoundaryFreqs[i]
            let upperFreq = octaveBoundaryFreqs[i+1]
            
            let mags = self.magsInFreqRange(lowerFreq, upperFreq)
            let ratio =  Float(mags.count) / Float(SpectrumFFT.numBandsPerOctave)
            
            // Now that we have the magnitudes within this octave, cluster them into bandsPerOctave groups and average each group.
            for j in 0..<SpectrumFFT.numBandsPerOctave {
                let startIdx = Int(ratio * Float(j))
                var stopIdx = Int(ratio * Float(j+1)) - 1 // inclusive
                
                stopIdx = max(0, stopIdx)
                
                if stopIdx <= startIdx {
                    self.bandMagnitudes.append(mags[startIdx])
                } else {
                    let avg = fastAverage(mags, startIdx, stopIdx + 1)
                    self.bandMagnitudes.append(avg)
                }
                
                let startMagnitudesIdx = Int(lowerFreq / self.bandwidth) + startIdx
                let endMagnitudesIdx = startMagnitudesIdx + (stopIdx - startIdx)
                self.bandFrequencies.append(self.averageFrequencyInRange(startMagnitudesIdx, endMagnitudesIdx))
            }
        }
        
        self.numberOfBands = self.bandMagnitudes.count
        self.bandMinFreq = self.bandFrequencies[0]
        self.bandMaxFreq = self.bandFrequencies.last
        SpectrumFFT.bandMagnitudeObject.addBands(bands: self.bandMagnitudes)
    }
    
    /// Same as above but with pre-calculated bands that try to mimic actual piano notes?
    func calculateNoteBands() {

        self.bandMagnitudes = [Float]()
        self.bandFrequencies = [Float]()
        
        // Break up the spectrum by octave
        for i in 0..<midPointRange.count - 1 {
            let lowerFreq = midPointRange[i]
            let upperFreq = midPointRange[i+1]
            
            let mags = self.magsInFreqRange(lowerFreq, upperFreq)
            
            let startIdx = 0
            let stopIdx = mags.count - 1 // inclusive
                
            let avg = fastAverage(mags, startIdx, stopIdx + 1)
            self.bandMagnitudes.append(avg)
            self.bandFrequencies.append((midPointRange[i] + midPointRange[i+1])/2)
        }
        
        self.numberOfBands = self.bandMagnitudes.count
       // self.bandMinFreq = self.bandFrequencies[0]
       // self.bandMaxFreq = self.bandFrequencies.last
        
    }
    
    private func magIndexForFreq(_ freq: Float) -> Int {
        return Int(Float(self.magnitudes.count) * freq / self.nyquistFrequency)
    }
    
    // On arrays of 1024 elements, this is ~35x faster than an iterational algorithm. Thanks Accelerate.framework!
    @inline(__always) private func fastAverage(_ array:[Float], _ startIdx: Int, _ stopIdx: Int) -> Float {
        var mean: Float = 0
        let ptr = UnsafePointer<Float>(array)
        vDSP_meanv(ptr + startIdx, 1, &mean, UInt(stopIdx - startIdx))
        
        return mean
    }
    
    // On arrays of 1024 elements, this is ~35x faster than an iterational algorithm. Thanks Accelerate.framework!
    @inline(__always) private func fastSum(_ array:[Float], _ startIdx: Int, _ stopIdx: Int) -> Float {
        var mean: Float = 0
        let ptr = UnsafePointer<Float>(array)
       // vDSP_meanv(ptr + startIdx, 1, &mean, UInt(stopIdx - startIdx))
        vDSP_sve(ptr + startIdx, 1, &mean, UInt(stopIdx - startIdx))
        return mean
    }
    
    @inline(__always) private func magsInFreqRange(_ lowFreq: Float, _ highFreq: Float) -> [Float] {
        let lowIndex = Int(lowFreq / self.bandwidth)
        var highIndex = Int(highFreq / self.bandwidth)
        
        if (lowIndex == highIndex) {
            // Occurs when both params are so small that they both fall into the first index
            highIndex += 1
        }
        
        return Array(self.magnitudes[lowIndex..<highIndex])
    }
    
    @inline(__always) private func averageFrequencyInRange(_ startIndex: Int, _ endIndex: Int) -> Float {
        return (self.bandwidth * Float(startIndex) + self.bandwidth * Float(endIndex)) / 2
    }
    
    /// Get the magnitude for the specified frequency band.
    /// - Parameter inBand: The frequency band you want a magnitude for.
    func magnitudeAtBand(_ inBand: Int) -> Float {
        assert(hasPerformedFFT, "*** Perform the FFT first.")
        assert(bandMagnitudes != nil, "*** Call calculateLinearBands() or calculateLogarithmicBands() first")
        
        return bandMagnitudes[inBand]
    }
    
    /// Get the magnitude of the requested frequency in the spectrum.
    /// - Parameter inFrequency: The requested frequency. Must be less than the Nyquist frequency (```sampleRate/2```).
    /// - Returns: A magnitude.
    func magnitudeAtFrequency(_ inFrequency: Float) -> Float {
        assert(hasPerformedFFT, "*** Perform the FFT first.")
        let index = Int(floorf(inFrequency / self.bandwidth ))
        return self.magnitudes[index]
    }
    
    /// Get the middle frequency of the Nth band.
    /// - Parameter inBand: An index where 0 <= inBand < size / 2.
    /// - Returns: The middle frequency of the provided band.
    func frequencyAtBand(_ inBand: Int) -> Float {
        assert(hasPerformedFFT, "*** Perform the FFT first.")
        assert(bandMagnitudes != nil, "*** Call calculateLinearBands() or calculateLogarithmicBands() first")
        return self.bandFrequencies[inBand]
    }
    
    /// Calculate the average magnitude of the frequency band bounded by lowFreq and highFreq, inclusive
    func averageMagnitude(lowFreq: Float, highFreq: Float) -> Float {
        
        var curFreq = lowFreq
        var total: Float = 0
        var count: Int = 0
        while curFreq <= highFreq {
            total += magnitudeAtFrequency(curFreq)
            curFreq += self.bandwidth
            count += 1
        }
        
        return total / Float(count)
    }
    
    /// Sum magnitudes across bands bounded by lowFreq and highFreq, inclusive
    func sumMagnitudes(lowFreq: Float, highFreq: Float, useDB: Bool) -> Float {
        
        var curFreq = lowFreq
        var total: Float = 0
        while curFreq <= highFreq {
            var mag = magnitudeAtFrequency(curFreq)
            if (useDB) {
                mag = max(0, SpectrumFFT.toDB(mag))
            }
            total += mag
            curFreq += self.bandwidth
        }
        
        return total
    }
    
    /// Sum magnitudes across bands bounded by lowFreq and highFreq, inclusive
    func sumMagnitudePositiveChanges() -> Float {
        
        if SpectrumFFT.lastBandMagnitudes == nil {
            SpectrumFFT.lastBandMagnitudes = bandMagnitudes
            return 0
        }
        
        var total: Float = 0
        let maxIndex = SpectrumFFT.lastBandMagnitudes.count < self.bandMagnitudes.count ?
                                SpectrumFFT.lastBandMagnitudes.count : self.bandMagnitudes.count
        for index in [Int](0..<maxIndex) {
            let currentMag = self.bandMagnitudes[index]
           // let lastMag = TempiFFT.lastBandMagnitudes[index]
            let lastAve = SpectrumFFT.bandMagnitudeObject.getBandAverage(band: index)
            var magChange = currentMag - lastAve
            magChange = max(0, SpectrumFFT.toDB(magChange))
            total += magChange
        }

        return total
    }
    
    /// A convenience function that converts a linear magnitude (like those stored in ```magnitudes```) to db (which is log 10).
    class func toDB(_ inMagnitude: Float) -> Float {
        // ceil to 128db in order to avoid log10'ing 0
        let magnitude = max(inMagnitude, 0.000000000001)
        return 10 * log10f(magnitude)
    }
}
