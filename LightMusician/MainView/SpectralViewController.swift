//
//  SpectralViewController.swift
//  TempiHarness
//
//  Created by John Scalo on 1/7/16.
//  Copyright © 2016 John Scalo. All rights reserved.
//

import UIKit
import AVFoundation
import CoreBluetooth
import ChameleonFramework
import MediaPlayer


enum InputSource: String {
    case microphone = "Microphone"
    case linein = "Line-In"
    case file = "filename"
}

let PatternName = ["Solid", "Rotating", "Two-Color"]

class SoundSource {
    var input = InputSource.microphone
    var item: MPMediaItem?
}

class SpectralViewController: UITableViewController, ScanDelegate,
UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ConnectionDialogDelegate {
    
    var audioInput: TempiAudioInput!
    @IBOutlet var amplitudeView: AmplitudeView!
    @IBOutlet var spectralView: SpectralView!

    @IBOutlet weak var patternNameLabel: UILabel!
    @IBOutlet weak var sensorTypeSegControl: UISegmentedControl!
    @IBOutlet weak var lightCountLabel: UILabel!
    @IBOutlet weak var lightControlSwitch: UISwitch!
    
    @IBOutlet weak var thresholdSlider: UISlider!
    @IBOutlet weak var thresholdValueLabel: UILabel!
    @IBOutlet weak var suppressionSlider: UISlider!
    @IBOutlet weak var supressionTimeLabel: UILabel!
    
    //Peripheral Controls
    @IBOutlet weak var fadeSlider: UISlider!
    @IBOutlet weak var fadeValue: UILabel!
    @IBOutlet weak var groupSegmentedControl: UISegmentedControl!
    @IBOutlet weak var schemeCollectionView: UICollectionView!
    @IBOutlet weak var frequencyRangeSlider: WARangeSlider!
    @IBOutlet weak var lowFreqencyLabel: UILabel!
    @IBOutlet weak var highFreqencyLabel: UILabel!
    
    static var NUM_AVE_SAMPLES = 1
    static var START_NUM_AVE_SAMPLES = 1
    static var NUM_GRAPH_SAMPLES = 300
    var currentMagnitudes = [Float](repeating: 0.0, count: SpectralViewController.NUM_AVE_SAMPLES)
    var graphMagnitudes = [Float](repeating: 0.0, count: SpectralViewController.NUM_GRAPH_SAMPLES)
    var currentMagPosition = 0
    var graphMagPosition = 0
    var currentMagMovingAverage: Float = 0.0
    var graphMagMovingAverage: Float = 0.0
    
    var inLightProcess = false
    var isNegativeBeat = false
    var isOffsetBeat = false
    var beatCount = 0
    var isProcessing = false
    
    static let NUM_PEAKS = 2
    var peakList = [Float](repeating: 0.0, count: SpectralViewController.NUM_PEAKS)
    var components = UIColor.green.cgColor.components
    var numBands = 200
    var filterBands = -1
    var countNonBeats = 0
    var resetBeatCount = false
    //playState turns of listening and controling, isLighting just stops controlling
    //playState is for backgrounding app, i.e when Phone is being used.
    var playState = true
    var isLighting = true
    
    var currentSettings: NSOrderedSet?
    var currentSettingIndex = 0
    
    var soundSource = SoundSource()
    let mp = MPMusicPlayerController.systemMusicPlayer
    
    var bleManager:BLEManager?

    let lightControl = LightControl.lightControl
    
    static var spectralViewController:SpectralViewController?
    static var peripheralToLightDict: [String: Light]?
    var selectedPeripheralDict = [UUID:BLEPeripheral]()

    override func viewDidLoad() {
        SpectralViewController.spectralViewController = self
        super.viewDidLoad()
        
        amplitudeView.backgroundColor = UIColor.black
        spectralView.backgroundColor = UIColor.black
        SpectrumFFT.initializeOctaveFrequencyRange()
        
        currentSettings = AppDelegate.rootDataObject?.trixySettings
        let audioInputCallback: TempiAudioInputCallback = { (timeStamp, numberOfFrames, samples) -> Void in
            self.gotSomeAudio(timeStamp: Double(timeStamp), numberOfFrames: Int(numberOfFrames), samples: samples)
        }
        
        audioInput = TempiAudioInput(audioInputCallback: audioInputCallback, sampleRate: Float(usedSampleRate), numberOfChannels: 1)

        setPlaying()
    }

    override func viewWillAppear(_ animated: Bool) {
        BLEPeripheralInterface.peripheralInterface!.dialogDelegate = self

        if AppDelegate.rootDataObject?.connectionTypeTrixy == true {
            currentSettings = AppDelegate.rootDataObject?.trixySettings
            sensorTypeSegControl.selectedSegmentIndex = 0
            currentSettingIndex = 0
        } else {
            currentSettings = AppDelegate.rootDataObject?.lightSettings
            sensorTypeSegControl.selectedSegmentIndex = 1
            if currentSettingIndex >= (AppDelegate.rootDataObject?.lightSettings?.count)! {
                currentSettingIndex = 0
            }
        }
        
        setPeripheralLabels()
        updateControls()
        tableView.reloadData()
     
        if (AppDelegate.rootDataObject?.isLighting)! {
            isLighting = true
            lightControlSwitch.isOn = true
        } else {
            isLighting = false
            lightControlSwitch.isOn = false
        }
        
        tableView.backgroundColor = Color.darkBackground.value
        schemeCollectionView.reloadData()
    }
    
    func showConnectingDialogs(hasBluetooth: Bool) {
        if hasBluetooth {
            showConnectingDialog()
        } else {
            showNoBluetoothPopup(dialogDelegate: self)
        }
    }
    
    func dialogDismissed() {
        setPeripheralLabels()
        if BLEManager.bleManager?.peripheralArray.count == 0 {
            showNoPeripheralPopup()
        }
    }
    
    func ignoreChosen() {
        //do nothing
    }
    
    func tryAgainChosen() {
        scan()
    }
    
    func setPeripheralLabels() {
        var trixyPeripherals = [BLEPeripheral]()
        var lightPeripherals = [BLEPeripheral]()

        for peripheral in BLEManager.bleManager!.peripheralArray {
            let prefix = BLEManager.peripheralNamePrefix[0]
            if let name = peripheral.getName() {
                if name.hasPrefix(prefix) {
                    trixyPeripherals.append(peripheral)
                } else {
                    lightPeripherals.append(peripheral)
                }
            }
        }
        let currentConnectedLights = AppDelegate.rootDataObject?.getConnectedLightPeripherals()
        let lightsFound = currentConnectedLights!.count + lightPeripherals.count
        let currentConnectedTrixy = AppDelegate.rootDataObject?.getConnectedTrixyPeripherals()
        var addRepeat = (currentConnectedTrixy?.count)!
        if addRepeat > 0 {
            for trixy in trixyPeripherals {
                if trixy.getName() == currentConnectedTrixy![0].getName() {
                    addRepeat = 0
                }
            }
        }
        let trixysFound = addRepeat + trixyPeripherals.count
        
        lightCountLabel.isHidden = true
        if sensorTypeSegControl.selectedSegmentIndex == 0 {
            lightCountLabel.isHidden = false
            if trixysFound == 0 {
                lightCountLabel.text = "None Found"
            } else if trixysFound == 1 {
                BLEManager.bleManager?.connectToPeripheral(blePeripheral: trixyPeripherals[0])
                lightCountLabel.text = trixyPeripherals[0].getName()
                if let trixySetting = AppDelegate.rootDataObject?.trixySettings![0] as? LightSettings {
                    if trixySetting.lights?.count == 0 {
                        let newLight = Light(context: AppDelegate.managedObjectContext!)
                        trixySetting.addToLights(newLight)
                    }
                    if let light = trixySetting.lights![0] as? Light {
                        light.peripheral = trixyPeripherals[0]
                        light.name = light.peripheral?.getName()
                    }
                }
            } else if trixysFound > 1 {
                lightCountLabel.text = "Multiple"
            }
        } else {
            if lightsFound == 0 {
#if !targetEnvironment(simulator)
                lightCountLabel.text = "None Found"
                lightCountLabel.isHidden = false
#endif
            }
        }
    }
    
    @IBAction func rescanForPeripherals(_ sender: Any) {
        scan()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         amplitudeView.setScale()
        schemeCollectionView.reloadData()
        saveContext()
    }
    
    @IBAction func setSensorType(_ segControl: UISegmentedControl) {
        currentSettingIndex = 0
        if segControl.selectedSegmentIndex == 0 {
            AppDelegate.rootDataObject?.connectionTypeTrixy = true
            currentSettings = AppDelegate.rootDataObject?.trixySettings
        } else {
            AppDelegate.rootDataObject?.connectionTypeTrixy = false
            currentSettings = AppDelegate.rootDataObject?.lightSettings
        }
        setPeripheralLabels()
        updateControls()
        tableView.reloadData()
        saveContext()
    }
    
    @IBAction func playControlTouched(_ sender: Any) {
        if let onSwitch = sender as? UISwitch {
            isLighting = onSwitch.isOn
            AppDelegate.rootDataObject?.isLighting = isLighting
            saveContext()
        }
    }
    //update control settings to current setting object
    func updateControls() {
        groupSegmentedControl.removeAllSegments()
      //  let title = "All"
      //  groupSegmentedControl.insertSegment(withTitle: title, at: 0, animated: false)
        for index in [Int](0..<(currentSettings?.count)!) {
            let title = (currentSettings![index] as! LightSettings).name
            groupSegmentedControl.insertSegment(withTitle: title, at: index, animated: false)
        }
        groupSegmentedControl.selectedSegmentIndex = currentSettingIndex
        
        if let currentSetting = currentSettings![currentSettingIndex] as? LightSettings {

            patternNameLabel.text = PatternName[Int(currentSetting.lightPattern)]
            
            thresholdSlider.value = currentSetting.threshold
            thresholdValueLabel.text = String(format: "%d", Int(thresholdSlider.value * 100))
            suppressionSlider.value = currentSetting.suppression
            supressionTimeLabel.text = String(format: "%.2f", currentSetting.suppression)
            fadeSlider.value = currentSetting.fade
            fadeValue.text = String(format: "%.2f", currentSetting.fade)

//            frequencyRangeSlider.lowerValue = Double(currentSetting.lowFrequency)
//            lowFreqencyLabel.text = String(format: "%d", currentSetting.lowFrequency)
//            frequencyRangeSlider.upperValue = Double(currentSetting.highFrequency)
//            highFreqencyLabel.text = String(format: "%d", currentSetting.highFrequency)
            schemeCollectionView.reloadData()
        }
    }
    
    @IBAction func updateCurrentGroup(_ groupSegControl: UISegmentedControl) {
        currentSettingIndex = groupSegControl.selectedSegmentIndex
        updateControls()
        tableView.reloadData()
        schemeCollectionView.reloadData()
    }
    
    func thresholdValueChanged(_ slider: UISlider) {
        if let currentSetting = currentSettings![currentSettingIndex] as? LightSettings {
            currentSetting.threshold = slider.value
            thresholdValueLabel.text = String(format: "%d", Int(slider.value * 100))
        }
    }
    
    func suppressionValueChanged(_ slider: UISlider) {
        if let currentSetting = currentSettings![currentSettingIndex] as? LightSettings {
            currentSetting.suppression = slider.value
            supressionTimeLabel.text = String(format: "%.2f", slider.value)
        }
    }
    
    func fadeValueChanged(_ slider: UISlider) {
        if let currentSetting = currentSettings![currentSettingIndex] as? LightSettings {
            currentSetting.fade = slider.value
            fadeValue.text = String(format: "%.2f", slider.value)
            currentSetting.calculateFadeValues()
        }
    }
    
    @IBAction func onSliderValChanged(slider: UISlider, event: UIEvent) {
        switch slider {
        case thresholdSlider:
            thresholdValueChanged(slider)
        case suppressionSlider:
            suppressionValueChanged(slider)
        case fadeSlider:
            fadeValueChanged(slider)
        default:
             break
        }
        saveContext()
    }
    
    @IBAction func rangeSliderChanged(_ rangeSlider: RangeSlider) {
        if let currentSetting = currentSettings![currentSettingIndex] as? LightSettings {
      //      currentSetting.lowFrequency = Int16(frequencyRangeSlider.lowerValue)
       //     currentSetting.highFrequency = Int16(frequencyRangeSlider.upperValue)
      //      lowFreqencyLabel.text = String(format: "%d", Int(frequencyRangeSlider.lowerValue))
       //     highFreqencyLabel.text = String(format: "%d", Int(frequencyRangeSlider.upperValue))
            saveContext()
        }
    }
    
    func setPlaying() {
        if playState {
            if soundSource.input == .microphone {
                audioInput.startRecording()
            } else {
                let collection = MPMediaItemCollection(items: [soundSource.item!])
                mp.setQueue(with: collection)
                mp.play()
            }
        } else {
            if soundSource.input == .microphone {
                audioInput.stopRecording()
            } else {
                mp.pause()
            }
        }
    }
    
    @objc func scan() {
      //  tricky here would be to scan only for the current sensor, but no...
      //  BLEManager.peripheralIDArray = BLEPeripheralInterface.peripheralIDArray
        BLEPeripheralInterface.peripheralInterface?.dialogDelegate = self
        BLEPeripheralInterface.peripheralInterface?.scanDelegate = self
        BLEPeripheralInterface.peripheralInterface?.doConnection()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTrixyPeripheralsID" {
            if let destination = segue.destination as? TrixyConnectionViewController {
                destination.currentSettings = currentSettings
                destination.currentSettingIndex = currentSettingIndex
            }
        } else if segue.identifier == "showPatternChoiceSegueID" {
            if let destination = segue.destination as? PatternViewController {
                destination.currentSettings = currentSettings
                destination.currentSettingIndex = currentSettingIndex
            }
        } else if segue.identifier == "setColorSchemeViewID" {
            if let destination = segue.destination as? SchemeDetailController {
                destination.currentSetting = (currentSettings![currentSettingIndex] as! LightSettings)
            }
        }
    }

    func endScan() {
        if let array = bleManager?.peripheralArray {
            if array.count > 1 {
                performSegue(withIdentifier: "scanForTrixyPeripheralsID", sender: nil)
            } else if (bleManager?.peripheralArray.count)! == 0 {
                //report none found
            }
        }
    }
    
    //for color scheme
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        let currentSetting = currentSettings![currentSettingIndex] as! LightSettings

        let numItems = CGFloat(currentSetting.numColors)
        var width = (collectionView.bounds.width - (2.0 * (numItems - 1.0))) / numItems
        var height = collectionView.bounds.height - 2
        let lb = CGFloat(Int(width))
        width = min(lb, 40)
        height = max(height, 0)

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let currentSetting = currentSettings![currentSettingIndex] as! LightSettings
        return Int(currentSetting.numColors)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorViewCellID", for: indexPath)
        
        // Configure the cell
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.black.cgColor
        let currentSetting = currentSettings![currentSettingIndex] as! LightSettings
        let color = currentSetting.colors![indexPath.item] as! ColorSet
        cell.backgroundColor = UIColor(red: CGFloat(color.red)/255.0, green: CGFloat(color.green)/255.0, blue: CGFloat(color.blue)/255.0, alpha: 1.0)
        cell.layer.cornerRadius = 3
        
        return cell
    }
}

extension UIViewController {
    func saveContext () {
        let context = AppDelegate.appDelegate?.persistentContainer.viewContext
        if (context?.hasChanges)! {
            do {
                try context?.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}


