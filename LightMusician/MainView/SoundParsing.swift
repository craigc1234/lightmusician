//
//  SoundParsing.swift
//  LightMusician
//
//  Created by Aardvark on 2/21/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import Foundation

extension SpectralViewController {
    func gotSomeAudio(timeStamp: Double, numberOfFrames: Int, samples: [Float]) {
        if playState == false {
            return
        }
        if isProcessing {
            print("isProcessing")
            return
        }
        isProcessing = true
        var maxAmplitudeForDrawing: Float = 0.0
        if !spectralView.fftInUse {
            spectralView.fftBeingUpdated = true
            let fft = SpectrumFFT(withSize: numberOfFrames, sampleRate: Float(usedSampleRate))
            fft.windowType = TempiFFTWindowType.hanning
            fft.fftForward(samples)
            fft.calculateLogarithmicBands(minFrequency: 100, maxFrequency: 6400)
            spectralView.fft = fft
            maxAmplitudeForDrawing = fft.getSumAmplitude(samples)
            spectralView.fftBeingUpdated = false
        } else {
            isProcessing = false
            return
        }
        let currentTime = Date.timeIntervalSinceReferenceDate
        var lightSettingsIndex = 0
        for lightSettingsObj in self.currentSettings! {
            if let lightSettings = lightSettingsObj as? LightSettings {
                let lastTime = lightSettings.lastBeatTime
                let isBeat = calculateIfBeatBandDiff(lightSettings, time: currentTime, lastTime: lastTime)
                if isBeat {
                    lightSettings.lastBeatTime = currentTime
                    //    SpectrumFFT.bandMagnitudeObject.printMagnitudes()
                }
                if currentSettingIndex == lightSettingsIndex {
                    //figure out drawing part
                    let graphAverage = calculateMovingAverageGraph(newValue: maxAmplitudeForDrawing)

                    let maxAmpThreshold = lightSettings.lastMaxAmplitude * lightSettings.threshold
                    let couldBeBeat = maxAmplitudeForDrawing > lightSettings.lastMaxAmplitude
                    let nonSmoothedBeat = maxAmplitudeForDrawing > maxAmpThreshold

                    tempi_dispatch_main { () -> () in
                        self.amplitudeView.addMagnitude(newMag: maxAmplitudeForDrawing, newAverage: graphAverage,
                                                        isBeat: isBeat, couldBeBeat: couldBeBeat, nonSmoothedBeat: nonSmoothedBeat, isNegativeBeat: self.isNegativeBeat)
                        self.amplitudeView.setNeedsDisplay()
                        
                        self.spectralView.setNeedsDisplay()
                    }
                }
                lightSettingsIndex += 1
                if maxAmplitudeForDrawing > 0.01 {
                    DispatchQueue.global(qos: .default).async {
                        //control lights
                        if isBeat {
                            if self.isLighting {
                                self.lightControl.updateColor(lightSettings: lightSettings)
                                lightSettings.nextFadeInterval = lightSettings.fadeInterval
                                lightSettings.fadeCountLeft = lightSettings.numFades
                            }
                        } else {
                            let isTrixy = (AppDelegate.rootDataObject?.trixySettings![0] as! LightSettings) == lightSettings
                            if self.isLighting && !isTrixy && lightSettings.fadeCountLeft > 0 && !self.inLightProcess {
                                self.inLightProcess = true
                                let currentDate = currentTime
                                if lightSettings.lastBeatTime + lightSettings.nextFadeInterval < currentDate {
                                     lightSettings.nextFadeInterval += lightSettings.fadeInterval
                                    lightSettings.fadeCountLeft -= 1
                                    self.lightControl.fadeColors(lightSettings: lightSettings)
                                }
                                self.inLightProcess = false
                            }
                        }
                        lightSettings.lastMaxAmplitude = maxAmplitudeForDrawing
                    }
                }
            }
        }
        isProcessing = false
    }
    
    func calculateIfBeatBandDiff(_ lightSettings: LightSettings, time: TimeInterval, lastTime: TimeInterval) -> Bool {
        
        let elapsedTime = time - lightSettings.lastBeatTime
        let isBeat = elapsedTime - Double(lightSettings.suppression) > 0.0 ? SpectrumFFT.bandMagnitudeObject.calulateBeat(threshold: lightSettings.threshold) : false
        if isBeat {
            lightSettings.lastBeatTime = time
        }
        return isBeat
    }
    
    func calculateIfBeatSimple(_ lightSettings: LightSettings, maxAmplitude: Float) -> Bool {
        let maxAmpThreshold = lightSettings.lastMaxAmplitude * lightSettings.threshold
        //suppress all beats in suppression time period
        let elapsedTime = Date.timeIntervalSinceReferenceDate - lightSettings.lastBeatTime
        let isBeat = elapsedTime - Double(lightSettings.suppression) > 0.0 ?
            maxAmplitude > maxAmpThreshold : false

        return isBeat
    }
    
    func isAboveThresholdPercent(newValue: Float, thresholdPercent: Float) -> Bool {
        //subtract lifo value, replace with new value
        currentMagnitudes[currentMagPosition] = newValue
        graphMagnitudes[currentMagPosition] = newValue
        currentMagPosition += 1
        graphMagPosition += 1
        if currentMagPosition == SpectralViewController.NUM_AVE_SAMPLES {
            currentMagPosition = 0
        }
        
        if graphMagPosition == SpectralViewController.NUM_GRAPH_SAMPLES {
            graphMagPosition = 0
        }
        
        var aboveThreshold = false
        var maxPeak: Float = 0.0
        
        for x in 0..<SpectralViewController.NUM_AVE_SAMPLES {
            let current = currentMagnitudes[x]
            if current > maxPeak {
                maxPeak = current
            }
        }
        
        if newValue > maxPeak * thresholdPercent {
            aboveThreshold = true
        }
        return aboveThreshold
    }
    
    func calculateMovingAverageThreshold(newValue: Float) -> Float {
        //subtract lifo value, replace with new value
        currentMagMovingAverage -= currentMagnitudes[currentMagPosition]
        let newAveValue = newValue/Float(SpectralViewController.NUM_AVE_SAMPLES)
        currentMagnitudes[currentMagPosition] = newAveValue
        currentMagMovingAverage += newAveValue
        currentMagPosition += 1
        if currentMagPosition == SpectralViewController.NUM_AVE_SAMPLES {
            currentMagPosition = 0
        }
        return currentMagMovingAverage
    }
    
    func calculateMovingAverageGraph(newValue: Float) -> Float {
        //subtract lifo value, replace with new value
        graphMagMovingAverage -= graphMagnitudes[graphMagPosition]
        let newAveValue = newValue/Float(SpectralViewController.NUM_GRAPH_SAMPLES)
        graphMagnitudes[graphMagPosition] = newAveValue
        graphMagMovingAverage += newAveValue
        graphMagPosition += 1
        if graphMagPosition == SpectralViewController.NUM_GRAPH_SAMPLES {
            graphMagPosition = 0
        }
        return graphMagMovingAverage
    }
}
