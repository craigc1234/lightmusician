//
//  SpectralViewTableController.swift
//  LightMusician
//
//  Created by Aardvark on 2/21/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import Foundation
import UIKit

extension SpectralViewController {
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            if let button = header.viewWithTag(10) {
                button.removeFromSuperview()
            }
            if section == 1 {
                if (AppDelegate.rootDataObject?.connectionTypeTrixy)! {
                    header.textLabel!.text = "TRX-1 Controls"
                } else {
                    header.textLabel!.text = "Light Controls"
                }
            }
            header.textLabel?.textAlignment = .center
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if AppDelegate.rootDataObject!.connectionTypeTrixy {
            if indexPath.section == 1 && indexPath.row == 0 {
                return 0
            } else if indexPath.section == 2 && indexPath.row == 2 {
                return 0
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                if (AppDelegate.rootDataObject?.lightSettings!.count)! < 2 {
                    return 0
                }
            }
        }

        //hide frequency range for now
        if indexPath.section == 1 && indexPath.row == 3 {
            return 0
        }
            
        let val = super.tableView(tableView, heightForRowAt: indexPath)
        return val
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 0 && indexPath.row == 0 {
            if sensorTypeSegControl.selectedSegmentIndex == 0 {
                performSegue(withIdentifier: "showTrixyPeripheralsID", sender: nil)
            } else {
                performSegue(withIdentifier: "showLightGroupsViewID", sender: nil)
            }
        }
    }
}
