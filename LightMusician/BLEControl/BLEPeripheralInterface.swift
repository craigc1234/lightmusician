//
//  SettingsDetailController.swift
//  TrickyBox
//
//  Created by Aardvark on 4/30/18.
//  Copyright © 2018 Luxapel. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreData

let kBoxConnectedNotification = Notification.Name(rawValue: "BoxConnectedNotification")
let kBoxUnconnectedNotification = Notification.Name(rawValue: "BoxUnconnectedNotification")
let kLightConnectedNotification = Notification.Name(rawValue: "LightConnectedNotification")
let kLightUnconnectedNotification = Notification.Name(rawValue: "LightUnconnectedNotification")


@objc protocol ScanDelegate: class {
    @objc func endScan()
}

class BLEPeripheralInterface {

    static let peripheralIDArray = ["6E400001-B5A3-F393-E0A9-E50E24DCCA9E", "0000ffe0-0000-1000-8000-00805f9b34fb"]
    static let serviceIDText = ["6E400001-B5A3-F393-E0A9-E50E24DCCA9E", "0000ffe0-0000-1000-8000-00805f9b34fb"]
    static let characteristicIDText = ["6E400002-B5A3-F393-E0A9-E50E24DCCA9E", "FFE1"]
    static let namePrefix = ["ZT"]

    static var peripheralInterface: BLEPeripheralInterface?
    
    var bleManager: BLEManager?
    
    weak var scanDelegate: ScanDelegate?
    weak var dialogDelegate: ConnectionDialogDelegate?

    init() {
        bleManager = AppDelegate.bleManager
        BLEPeripheralInterface.peripheralInterface = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(endScan),
                                               name: kScanStoppedNotification, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(peripheralListChanged),
                                               name: kPeripheralFoundNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(peripheralListChanged),
                                               name: kPeripheralDisconnectedNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(peripheralListChanged),
                                               name: kPeripheralNotFoundNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(lightConnected),
                                               name: kPeripheralConnectedNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(characteristicsDiscovered),
                                               name: kCharacteristicsFoundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(bluetoothReconnected),
                                               name: kCentralManagerOnNotification, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(bluetoothDisconnected),
                                               name: kCentralManagerOffNotification, object: nil)
        
    
        BLEManager.peripheralIDArray = BLEPeripheralInterface.peripheralIDArray
        BLEManager.serviceIDText = BLEPeripheralInterface.serviceIDText
        BLEManager.characteristicIDText = BLEPeripheralInterface.characteristicIDText
        BLEManager.peripheralNamePrefix = BLEPeripheralInterface.namePrefix
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func bluetoothDisconnected(notification: NSNotification) {
        doConnection()
    }
    
    @objc func bluetoothReconnected(notification: NSNotification) {
        doConnection()
    }
    
    func doConnection() {
        if testIfBluetooth() {
            startScan()
            dialogDelegate?.showConnectingDialogs(hasBluetooth: true)
        } else {
            dialogDelegate?.showConnectingDialogs(hasBluetooth: false)
        }
    }
    
    func dialogDismissed() {
        dialogDelegate?.dialogDismissed()
    }
    
    func testIfBluetooth() -> Bool {
        return BLEManager.bleManager?.centralManager?.state != .poweredOff &&
            BLEManager.bleManager?.centralManager?.state != .unsupported
    }

    func startScan(scanTime: Double = 2.0) {
        var peripheralUUIDArray = [CBUUID]()
        for id in BLEManager.peripheralIDArray {
            let peripheralUUID = CBUUID(string: id)
            peripheralUUIDArray.append(peripheralUUID)
        }
        bleManager?.scan(cbuuid: peripheralUUIDArray, prefix: BLEManager.peripheralNamePrefix)
    }
    
    func startScan(delegate: ScanDelegate, scanTime: Double = 2.0) {
        scanDelegate = delegate
        var peripheralUUIDArray = [CBUUID]()
        for id in BLEManager.peripheralIDArray {
            let peripheralUUID = CBUUID(string: id)
            peripheralUUIDArray.append(peripheralUUID)
        }
        bleManager?.scan(cbuuid: peripheralUUIDArray)
    }

    @objc func endScan() {
        let foundPeripherals = BLEManager.bleManager?.peripheralArray

        for peripheral in foundPeripherals! {
            bleManager?.connectToPeripheral(blePeripheral: peripheral)
        }
        
        if AppDelegate.rootDataObject?.unconnectedBoxes.count == 1 {
            let box = AppDelegate.rootDataObject?.unconnectedBoxes[0]
            (AppDelegate.rootDataObject?.trixySettings![0] as! LightSettings).removeFromLights(at: 0)
            (AppDelegate.rootDataObject?.trixySettings![0] as! LightSettings).addToLights(
                box!)
        } else if AppDelegate.rootCreated {
            AppDelegate.rootDataObject?.connectionTypeTrixy = false
        }

        scanDelegate?.endScan()
    }

    @objc func peripheralListChanged(notification: NSNotification) {
        if let _ = notification.object as? BLEPeripheral {
            NotificationCenter.default.post(name: kBoxUnconnectedNotification, object: nil, userInfo: nil)
        }
    }

    @objc func lightConnected(notification: NSNotification) {
        if let peripheral = notification.object as? BLEPeripheral {
            peripheral.startDiscoveringServices()
        }
    }

    @objc func characteristicsDiscovered(notification: NSNotification) {
        if let name = (notification.object as? BLEPeripheral)?.getName() {
            if let peripheral = notification.object as? BLEPeripheral {
                peripheral.connected = true
                if name.hasPrefix("ZT") {
                    recordBoxConnection(peripheral: peripheral)
                } else {
                    recordLightConnection(peripheral: peripheral)
                }
            }
        }
    }
    
    func recordBoxConnection(peripheral: BLEPeripheral) {

        peripheral.setWriteTrixyCharacteristic()

        NotificationCenter.default.post(name: kBoxConnectedNotification, object: nil, userInfo: nil)
    }

    func recordLightConnection(peripheral: BLEPeripheral) {
        let serviceIDString = BLEManager.serviceIDText[1]
        let characteristicIDString = BLEManager.characteristicIDText[1]

        let serviceID = CBUUID(string: serviceIDString)
        let characteristicID = CBUUID(string: characteristicIDString)
        
        peripheral.setWriteCharacteristic(service: serviceID,
                             characteristicID: characteristicID)
        
        let data = "So0001\n".data(using: String.Encoding.utf8)
        peripheral.writeDataToCharacteristic(data: data!, type: .write)
        AppDelegate.rootDataObject?.addPeripheral(peripheral: peripheral)

        NotificationCenter.default.post(name: kLightConnectedNotification, object: nil, userInfo: nil)
    }
    
    func writeToLights() {
        
    }
}

