//
//  BLEZoneInterface
//  TrickyBox
//
//  Interface between sequences/scenes and the BLECommands object
//  Created by Aardvark on 4/30/18.
//  Copyright © 2018 Luxapel. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreData

class BLEZoneInterface {

    var currentTimer: Timer?

    static func setZoneColor(color: UIColor, dmxAddress: Int, setImmediate: Bool) {
        let zoneIndex = Int((dmxAddress - 1) / 4)
        let zoneString = String(format: "%02X", zoneIndex)
        let rgbwString = toRGBW(color)
        var prefix = "c"
        if setImmediate {
            prefix = "C"
        }
        let command = prefix + zoneString + rgbwString
        
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setZoneColor(colorArray: [Int], dmxAddress: Int, setImmediate: Bool) {
        let zoneIndex = Int((dmxAddress - 1) / 4)
        var rgbwString = String(format: "%02X", colorArray[0])
        rgbwString += String(format: "%02X", colorArray[1])
        rgbwString += String(format: "%02X", colorArray[2])
        rgbwString += String(format: "%02X", colorArray[3])
        
        let zoneString = String(format: "%02X", zoneIndex)
        
        var prefix = "c"
        if setImmediate {
            prefix = "C"
        }
        let command = prefix + zoneString + rgbwString
        
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setFadeTime(time: Float) {
        let value: Float
        if time < 327.68 {
            value = time * 100.0
        } else {
            value = (time - 327.68) * 10 + 0x8000
        }

        let fadeTime = String(format: "%04X", Int(value))
        let command = "f" + fadeTime
        
        AppDelegate.bleCommander?.addCommand(command: command)
    }
/*
    I'm also going the change the minimum interval for time to 1/100 sec.  Values from 1 to 0x8000 are 1/100 sec, so .01 to 327.68 seconds.  Values from 0x8000 to 0xFFFE are (param-0x8000)/10 + 327.768 seconds.  So 0xFFFE is about an hour.  0xFFFF is a special reserved value.
    So to convert from time to parameter:
    
    if (time < 327.68)  param = time * 100;
    else param = (time - 327.68) * 10 + 0x8000
*/
 
    static func setDwellTime(time: Float) {
        
        let value: Float
        if time < 327.68 {
            value = time * 100.0
        } else {
            value = (time - 327.68) * 10 + 0x8000
        }
        let dwellTime = String(format: "%04X", Int(value))
        let command = "d" + dwellTime
        
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setLoopStart() {
        let command = "j"
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setFakeRepeatStart() {
        let command = "j"
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setLoopEnd(count: Int) {
        let countString = String(format: "%04X", count)
        let command = "k" + countString
        
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setRepeat() {
        let command = "r"
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func setFakeRepeat() {
        let countString = String(format: "%04X", 65000)
        let command = "k" + countString
        AppDelegate.bleCommander?.addCommand(command: command)
    }
    
    static func toRGBW(_ color: UIColor) -> String {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        var fWhite: CGFloat = 0
        if color.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            fRed = max(fRed, 0.0)
            fGreen = max(fGreen, 0.0)
            fBlue = max(fBlue, 0.0)
            fAlpha = max(fAlpha, 0.0)
            fWhite = max(fWhite, 0.0)
            let iRed = NSString(format: "%02X", Int(fRed * 255.0)) as String
            let iGreen = NSString(format: "%02X", Int(fGreen * 255.0)) as String
            let iBlue = NSString(format: "%02X", Int(fBlue * 255.0)) as String
            // let iAlpha = NSString(format: "%02X", Int(fAlpha * 255.0)) as String
            let iWhite = NSString(format: "%02X", Int(fWhite * 255.0)) as String
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            //let rgbw = (iRed << 24) + (iGreen << 16) + (iBlue << 8) + iWhite
            //return NSString(format: "%08X", rgbw) as String
            return iRed + iGreen + iBlue + iWhite
        } else {
            return "00000000"
        }
    }
}
