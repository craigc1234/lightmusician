//
//  BLEManager.swift
//
//  Created by Craig Conner on 1/24/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//
// Some background info at
//      https://medium.com/arkulpa/ios-stay-connected-to-an-external-ble-device-as-much-as-possible-699d434846d2
//

import Foundation
import CoreBluetooth

let kCentralManagerOnNotification = Notification.Name(rawValue: "CentralMangerOnNotification")
let kCentralManagerOffNotification = Notification.Name(rawValue: "CentralMangerOffNotification")
let kCentralManagerUpdateStateNotification = Notification.Name(rawValue: "CentralManagerUpdateStateNotification")

let kScanStartedNotification = Notification.Name(rawValue: "ScanStartedNotification")
let kScanStoppedNotification = Notification.Name(rawValue: "ScanStoppedNotification")

//peripheral discovered
let kPeripheralFoundNotification = Notification.Name(rawValue: "PeripheralFoundNotification")
//if allow duplicates during scan, notify if one went missing
let kPeripheralNotFoundNotification = Notification.Name(rawValue: "PeripheralNotFoundNotification")

//peripheral connected to, services and characteristics discovered
let kConnectionRequestedNotification = Notification.Name(rawValue: "ConnectionRequestedNotification")
let kPeripheralConnectedNotification = Notification.Name(rawValue: "PeripheralConnectedNotification")
let kCharacteristicsFoundNotification = Notification.Name(rawValue: "CharacteristicsFoundNotification")
let kDisconnectionRequestedNotification = Notification.Name(rawValue: "DisconnectionRequestedNotification")
let kPeripheralDisconnectedNotification = Notification.Name(rawValue: "PeripheralDisconnectedNotification")

let kWriteOutputNotification = Notification.Name(rawValue: "WriteOutputNotification")

let kPeripheralArrayUpdatedNotification = Notification.Name(rawValue: "kPeripheralArrayUpdatedNotification")


class BLEManager: NSObject, CBCentralManagerDelegate {

    static var bleManager: BLEManager?
    var centralManager: CBCentralManager?
    
    //default trixy version 1 values
    static var peripheralIDArray = ["0000ffe0-0000-1000-8000-00805f9b34fb"]
    static var serviceIDText = ["FFE0"]
    static var characteristicIDText = ["FFE1"]
    static var peripheralNamePrefix = ["ZX"]

    //When we find a peripheral for the first time, we just keep that in the transient peripherals dict/array
    //It would be nice if the UUID were a CBUUID, but CBPeripheral has a UUID, so....
    //one to enumerate them, one for lookup
    var peripheralDict =  [UUID: BLEPeripheral]()
    var peripheralArray = [BLEPeripheral]()

    var currentTimer: Timer?
    //interval for fixed scan length
    var interval: TimeInterval = 3
    //intervals for scanning with duplicates, longer one to begin, then
    var startingSweepInterval: TimeInterval = 10
    var sweepInterval: TimeInterval = 10
    var continuingSweepInterval: TimeInterval = 3
    var allowDuplicates = false
    var reconnectWhenLost = false
    var firstConnection = true

    //this could logically belong to each BLEPeripheral, except for the current
    //sweep mechanism is based on other actions occasionally happening, which
    //wouldn't happen if some one light was dead - might change this later
    let bleQueue = BLEQueue(queueWaitTime: 0.0)
    
    //queue used by color wheel to limit commands sent, only keep last
    let colorWheelQueue = BLEQueue(queueWaitTime: 0.2, queueMaxCount: 1)

    class func getPeripheral(peripheralID: UUID) -> BLEPeripheral? {
        return BLEManager.bleManager?.peripheralDict[peripheralID]
    }

    class func getCharacteristic(peripheralID: UUID,
                                 serviceID: CBUUID,
                                 characteristicID: CBUUID) -> CBCharacteristic? {
        if let peripheral = BLEManager.bleManager?.peripheralDict[peripheralID] {
            return peripheral.getCharacteristicByID(serviceID: serviceID, charID: characteristicID)
        }
        return nil
    }

    override init() {
        super.init()
        BLEManager.bleManager = self

        let centralQueue = DispatchQueue(label: AppDelegate.dispatchQueueName, attributes: [])
        centralManager = CBCentralManager(delegate: self, queue: centralQueue)
    }

    func getPeripheral(uuid: UUID) -> BLEPeripheral? {
        if let peripheral = peripheralDict[uuid] {
            return peripheral
        }
        return nil
    }

    func getPeripheral(byIndex: Int) -> BLEPeripheral? {
        if peripheralArray.count > byIndex {
            return peripheralArray[byIndex]
        }
        return nil
    }

    func removePeripheral(uuid: UUID) {
        if let peripheral = peripheralDict[uuid] {
            disconnectFromPeripheral(blePeripheral: peripheral)
            peripheralDict.removeValue(forKey: peripheral.getID()!)
        }
    }

    func removePeripheral(byIndex: Int) {
        if peripheralArray.count > byIndex {
            let peripheral = peripheralArray.remove(at: byIndex)
            peripheralDict.removeValue(forKey: (peripheral.getID())!)
        }
    }

    func scan(cbuuid: [CBUUID], prefix: [String]) {
        BLEManager.peripheralNamePrefix = prefix
        scan(cbuuid: cbuuid)
    }

    //scan for peripherals
    // if interval is default 15, assume first run, else assume we are trying to extend scan time
    //      so stop timer and restart with new value
    func scan(cbuuid: [CBUUID]? = nil) {
        allowDuplicates = false
        if let central = centralManager {
            if !(centralManager?.isScanning)! {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: kScanStartedNotification, object: nil, userInfo: nil)
                }
                peripheralDict = [UUID: BLEPeripheral]()
                peripheralArray = [BLEPeripheral]()
                DispatchQueue.global().async {
                    central.scanForPeripherals(withServices: cbuuid, options: nil)
                }

                currentTimer = Timer.scheduledTimer(timeInterval: interval,
                                        target: self, selector: #selector(BLEManager.endScan),
                                        userInfo: nil, repeats: false)
            }
        }
    }

    //scan for peripherals, allowing duplicates
    // if duplicate found, update time stamp
    //perform periodic sweep, if track if peripherals disappear
    func scanWithDuplicates(cbuuid: [CBUUID]? = nil) {
        if let central = centralManager {
            if !(centralManager?.isScanning)! {
                allowDuplicates = true
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: kScanStartedNotification, object: nil, userInfo: nil)
                }
                peripheralDict = [UUID: BLEPeripheral]()
                peripheralArray = [BLEPeripheral]()
                DispatchQueue.global().async {
                    central.scanForPeripherals(withServices: cbuuid,
                                    options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                }
                sweepInterval = startingSweepInterval
                currentTimer = Timer.scheduledTimer(timeInterval: sweepInterval,
                            target: self, selector: #selector(BLEManager.performSweep),
                            userInfo: nil, repeats: true)
            }
        }
    }

    //rescan - like scan with duplicates, but times out quickly
    func rescan(cbuuid: [CBUUID]? = nil) {
        if centralManager != nil && !(centralManager?.isScanning)! {
            scanWithDuplicates(cbuuid: cbuuid)

            currentTimer = Timer.scheduledTimer(timeInterval: 5,
                            target: self, selector: #selector(BLEManager.endScan),
                            userInfo: nil, repeats: false)
        }
    }

    @objc func performSweep() {
        //after longer first interval to find everything, shorten sweep to new value
        if sweepInterval != continuingSweepInterval {
            currentTimer?.invalidate()
            sweepInterval = continuingSweepInterval
            currentTimer = Timer.scheduledTimer(timeInterval: sweepInterval,
                                                target: self, selector: #selector(BLEManager.performSweep),
                                                userInfo: nil, repeats: true)
        }
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        var notFoundArray = [Int]()
        var currentIndex = 0
        for peripheral in peripheralArray {
            if peripheral.connected == false
                || peripheral.peripheral.state == .disconnected {
                peripheral.connected = false
                if currentTime - peripheral.timeStamp > sweepInterval {
                    //store peripheral for future removal
                    notFoundArray.append(currentIndex)
                }
            }
            currentIndex += 1
        }
        for indexToRemove in notFoundArray.reversed() {
            let peripheralToRemove = peripheralArray[indexToRemove]
            peripheralDict[peripheralToRemove.getID()!] = nil
            peripheralArray.remove(at: indexToRemove)
            //dispatch notification after removing from array/dict
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kPeripheralNotFoundNotification,
                                    object: peripheralToRemove, userInfo: nil)
            }
        }
    }

    @objc func endScan() {
        currentTimer?.invalidate()
        currentTimer = nil
        if let central = centralManager {
            central.stopScan()
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kScanStoppedNotification, object: nil, userInfo: nil)
            }
        }
    }

    func connectToPeripheral(blePeripheral: BLEPeripheral) {
        if blePeripheral.connected && blePeripheral.peripheral.state == .connected {
            return
        }
        centralManager?.connect(blePeripheral.peripheral, options: nil)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kConnectionRequestedNotification,
                                    object: blePeripheral, userInfo: nil)
        }
    }

    func disconnectFromPeripheral(blePeripheral: BLEPeripheral) {
        if let peripheral = peripheralDict[blePeripheral.peripheral.identifier] {
            peripheral.connected = false
        }
        centralManager?.cancelPeripheralConnection(blePeripheral.peripheral)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kDisconnectionRequestedNotification,
                                    object: blePeripheral, userInfo: nil)
        }
    }

    // MARK: - CBCentralManagerDelegate

    func centralManager(_ central: CBCentralManager, didDiscover
                        peripheral: CBPeripheral,
                        advertisementData: [String: Any],
                        rssi RSSI: NSNumber) {
        if let name = peripheral.name {
            print(name, peripheral.identifier)
        } else {
            return
        }

        // Be sure to retain the peripheral or it will fail during connection.
        var blePeripheral = peripheralDict[peripheral.identifier]
        if peripheralDict[peripheral.identifier] == nil {
            blePeripheral = BLEPeripheral(initWithPeripheral: peripheral,
                                advertisementData: advertisementData, rssi: RSSI)
            peripheralDict[peripheral.identifier] = blePeripheral!
            peripheralArray.append(blePeripheral!)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kPeripheralFoundNotification,
                                    object: blePeripheral, userInfo: nil)
            }
        }
        if allowDuplicates == true {
            //update timestamp
            blePeripheral?.timeStamp = NSDate.timeIntervalSinceReferenceDate
        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.readRSSI()
        if let blePeripheral = peripheralDict[peripheral.identifier] {
            blePeripheral.startDiscoveringServices()
            blePeripheral.connected = true
            print("Connected")
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kPeripheralConnectedNotification,
                                        object: blePeripheral, userInfo: nil)
            }
        }
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral
                        peripheral: CBPeripheral,
                        error: Error?) {
        if let error = error {
            print("error: \(error)")
        }
        print("Disconnecting")
        if let blePeripheral = peripheralDict[peripheral.identifier] {
            blePeripheral.connected = false
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kPeripheralDisconnectedNotification,
                                        object: blePeripheral, userInfo: nil)
            }

            if reconnectWhenLost {
                //this never times out, so should reconnect when back in range or bluetooth turned back on
                //modulo app stays running, of course.
                connectToPeripheral(blePeripheral: blePeripheral)
            }
        }
    }

    func clearDevice(peripheral: BLEPeripheral) {
        peripheralDict[peripheral.getID()!] = nil
        peripheral.connected = false
        if let index = peripheralArray.index(of: peripheral) {
            peripheralArray.remove(at: index)

        }
    }

    func clearDevices() {
        for peripheral in peripheralArray {
            if peripheral.connected {
                disconnectFromPeripheral(blePeripheral: peripheral)
                peripheral.connected = false
            }
        }
        peripheralDict = [UUID: BLEPeripheral]()
        peripheralArray = [BLEPeripheral]()
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOff:
            self.clearDevices()
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kCentralManagerOffNotification, object: nil, userInfo: nil)
            }
        case .unauthorized:
            break
        case .unknown:
            // Wait for another event
            break
        case .poweredOn:
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kCentralManagerOnNotification, object: nil, userInfo: nil)
            }
            firstConnection = false
            return
        case .resetting:
            self.clearDevices()
        case .unsupported:
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kCentralManagerOffNotification, object: nil, userInfo: nil)
            }
            break
        }
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kCentralManagerUpdateStateNotification,
                                            object: central.state, userInfo: nil)
        }
    }
}
