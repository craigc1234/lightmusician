//
//  BLEQueue.swift
//  LuxiumSwift
//
//  Created by Aardvark on 3/3/18.
//  Copyright © 2018 LogicCascade. All rights reserved.
//

import Foundation

let kQueueOutputNotification = Notification.Name(rawValue: "QueueOutputNotification")

enum CommandType {
    case command
    case response
    case wait
}

public struct Queue<T> {
    fileprivate var array = [T?]()
    fileprivate var head = 0
    
    public var isEmpty: Bool {
        return count == 0
    }

    public var count: Int {
        return array.count - head
    }

    public mutating func enqueue(_ element: T) {
        array.append(element)
    }

    public mutating func dequeue() -> T? {
        guard head < array.count, let element = array[head] else { return nil }

        array[head] = nil
        head += 1

        //clean up periodically, but not every time we take something off
        let percentage = Double(head)/Double(array.count)
        if array.count > 50 && percentage > 0.25 {
            array.removeFirst(head)
            head = 0
        }

        return element
    }

    public var front: T? {
        if isEmpty {
            return nil
        } else {
            return array[head]
        }
    }
}

class Element {
    var data: Data?
    var peripheral: BLEPeripheral
    var callback: ((String, _ expectedResponse: String?) -> Void)?
    var expectedResponse = ""
    var timeStamp: Double = 0.0
    var type = CommandType.command
    var waitTime = 0.0

    init(newPeripheral: BLEPeripheral) {
        peripheral = newPeripheral
    }

    init(newPeripheral: BLEPeripheral, newData: Data, newCallback: ((String, _ expectedResponse: String?) -> Void)? ) {
        peripheral = newPeripheral
        data = newData
        callback = newCallback
    }
}

class BLEQueue {
    var elements = Queue<Element>()

    //-1 is unlimited, set if desired
    //say, like a color wheel when only the last command is interesting, set to value of 1
    var maxCount = -1
    //waitTime is the time to delay before calling the next item in the queue
    var waitTime = 0.0
    //time allowed for response, before panicing.
    var maxResponseTime = 5.0
    //timer for whole queue
    var runQueueTimer: Timer?
    
    //specificially for wait commands
    var waitTimer: Timer?
    var lastRunTime = 0.0
    //if waiting for a response, store element here
    var waitingElement: Element?
    
    //sweep interval, if waiting for a response and it doesn't show, sweep
    //not used in present code
    var sweepInterval = 5.0
    var writeResponseDictionary = [UUID: TimeInterval]()

    init(queueWaitTime: Double, queueMaxCount: Int = -1) {
        waitTime = queueWaitTime
        maxCount = queueMaxCount
    }
    
    deinit {
        print("queue deinit")
    }

    func addElement(_ element: Element) {
        DispatchQueue.main.async {
            self.elements.enqueue(element)
            if self.maxCount > 0 && self.maxCount < self.elements.count {
                //if we reached our limit, remove the oldest, discarding it
                _ = self.elements.dequeue()
            }
            //immediately run this one, or make sure some one is running
            self.runFromQueue()
        }
    }

    //if there's something there, but nothing running, start the next one.
    func runFromQueue() {
        if runQueueTimer == nil && elements.count > 0 {
            if runNextItem() {
                runQueueTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                     selector: #selector(checkRunQueue),
                                     userInfo: nil, repeats: false)
            }
        }
    }

    @objc
    func checkRunQueue(notification: NSNotification) {
        runQueueTimer?.invalidate()
        runQueueTimer = nil
        runFromQueue()
    }

    //only run next item is no currently running one for this id
    func runNextItem() -> Bool {
        var response = true
        if let nextElement = elements.dequeue() {
            switch nextElement.type {
            case .wait:
                runQueueTimer = Timer.scheduledTimer(timeInterval: nextElement.waitTime, target: self,
                                                     selector: #selector(checkRunQueue),
                                                     userInfo: nil, repeats: false)
                response = false
                DispatchQueue.main.async {
                        NotificationCenter.default.post(name: kQueueOutputNotification,
                                                        object: nil, userInfo: ["output": "waiting"])
                }
            case .command:
                let str = String(data: nextElement.data!, encoding: .utf8)!
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: kQueueOutputNotification,
                                                    object: nil, userInfo: ["output": "running command: " + str])
                }
                nextElement.peripheral.writeDataToCharacteristic(data: nextElement.data!, type: .respond)
            case .response:
                let str = String(data: nextElement.data!, encoding: .utf8)!
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: kQueueOutputNotification,
                                                    object: nil,
                                                    userInfo: ["output": "running command with response: " + str])
                }
                nextElement.timeStamp = NSDate.timeIntervalSinceReferenceDate
                let uuid = (nextElement.peripheral.getID())!
                response = false
                runQueueTimer = Timer.scheduledTimer(timeInterval: maxResponseTime, target: self,
                                                 selector: #selector(sweepForIncompleteItems),
                                                 userInfo: uuid, repeats: false)
                self.writeResponseDictionary[uuid] = NSDate.timeIntervalSinceReferenceDate
                nextElement.peripheral.writeDataToCharacteristic(data: nextElement.data!, type: .respond)
            }
        }
        return response
    }

    //this isn't actually integrated (no waiting element). How you get here from a response is up in the air
    func itemCompleted(uuid: UUID, response: String) {
        DispatchQueue.main.async {
            let element = self.waitingElement
            if element?.callback != nil {
                element?.callback?(response, element?.expectedResponse)
            }
            self.writeResponseDictionary[uuid] = nil
            self.runFromQueue()
        }
    }

    @objc func sweepForIncompleteItems() {
        //sweep items to check for any not getting handled timely
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        for (uuid, startTime) in writeResponseDictionary where currentTime - startTime > sweepInterval {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: kQueueOutputNotification, object: nil,
                                                userInfo: ["output": "Error: No Response"])
            }
            if waitingElement?.type != .wait {
                writeResponseDictionary[uuid] = nil
                runFromQueue()
            }
            //else do something....
        }
    }

}
