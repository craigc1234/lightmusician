//
//  BLECommands.swift
//  TrickyBox
//
//  Created by Aardvark on 5/15/18.
//  Copyright © 2018 Luxapel. All rights reserved.
//

import UIKit


var date_start = NSDate()

class BLECommands {

    var currentPeripheral: BLEPeripheral?
    var peripheralIDText = "0000ffe0-0000-1000-8000-00805f9b34fb"
    var serviceIDText = "FFE0"
    var characteristicIDText = "FFE1"
    var waitInterval = 0.5
    var lastCommandTime = NSDate.timeIntervalSinceReferenceDate

    var transitionTime = 0

    var currentProgram = [String]()
    var currentProgramSize = 0
    static let MAX_SIZE = 1111
    var lastProgram = [String]()
    var programTimer: Timer?
    
    var printCommand = false

    var currentBufferSize = 0
    
    func addCommand(command: String) {
        currentProgramSize += 1
        if printCommand {
            print(command)
        }
        if currentPeripheral != nil && currentProgram.count > 0 {
            if currentBufferSize + command.count - 2 < (currentPeripheral?.maxMTUBuffer)! {
                if currentProgram[currentProgram.count - 1].count > 0 {
                    currentProgram[currentProgram.count - 1] += "\n"
                }
                currentProgram[currentProgram.count - 1] += command
                currentBufferSize += command.count + 1
            } else {
                currentProgram.append(command)
                currentBufferSize = command.count
            }
        } else {
            currentProgram.append(command)
            currentBufferSize = command.count
        }
    }
    
    func startNewProgram() {
        currentProgramSize = 0
        currentProgram = [String]()
    }
    
    func sendCurrentProgramNow() {
        sendBaseProgram()
    }

    func sendBaseProgram() {
        sendCommand(command: "B00")
        for command in currentProgram {
            sendCommand(command: command)
        }
        sendCommand(command: "X00")
    }
    
    func downloadCurrentProgram() {
        sendCommand(command: "B00")
        for command in currentProgram {
            sendCommand(command: command)
        }
    }
    
    func runDownloadedProgram() {
        sendCommand(command: "X00")
    }
    
    func sendLastProgramWithDelay() {
        sendCurrentProgramWithDelay()
    }

    @objc func sendDelayedProgram(notification: NSNotification) {
        sendCurrentProgram()
    }
    
    func sendCurrentProgram() {
        sendCommand(command: "B00")
        for command in currentProgram {
            sendCommand(command: command)
        }
        sendCommand(command: "X00")
    }
    
    //use only for immediate commands, only designed for color wheel
    func sendCurrentProgramWithDelay() {
        for command in currentProgram {
            sendColorCommand(command: command)
        }
    }
    
    func sendEmptyProgram() {
        sendCommand(command: "B00")
        sendCommand(command: "X00")
    }
    
    func pauseProgram() {
        sendCommand(command: "P")
    }
    
    func continueProgram() {
        sendCommand(command: "Q")
    }
    
    func continueProgramAfterLoop() {
        sendCommand(command: "R")
    }
    
    func sendStatusCommand() {
        sendCommand(command: "S")
    }
    
    //send command that needs to go. Use for settings, not for color switching.
    func sendColorCommand(command: String) {
        guard let peripheral = currentPeripheral else { return }
        
        let element = Element(newPeripheral: peripheral)
        element.type = .command
        var str = command
        str += "\n"
        
//        print("CW Command " + str, NSDate.timeIntervalSinceReferenceDate)
        element.data = str.data(using: .utf8)
        
        AppDelegate.bleManager?.colorWheelQueue.addElement(element)
    }
    
    //send command now. If you want control over timing, or how many, create a BLEQueue
    func sendCommand(command: String) {
        guard let peripheral = currentPeripheral else { return }

        let element = Element(newPeripheral: peripheral)
        element.type = .command
        var str = command
        str += "\n"
        
 //       print("Command " + str)
        element.data = str.data(using: .utf8)

        AppDelegate.bleManager?.bleQueue.addElement(element)
    }

    func toRGBW(_ color: UIColor) -> String {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        var fWhite: CGFloat = 0
        if color.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            fRed = max(fRed, 0.0)
            fGreen = max(fGreen, 0.0)
            fBlue = max(fBlue, 0.0)
            fAlpha = max(fAlpha, 0.0)
            fWhite = max(fWhite, 0.0)
            let iRed = NSString(format: "%02X", Int(fRed * 255.0)) as String
            let iGreen = NSString(format: "%02X", Int(fGreen * 255.0)) as String
            let iBlue = NSString(format: "%02X", Int(fBlue * 255.0)) as String
           // let iAlpha = NSString(format: "%02X", Int(fAlpha * 255.0)) as String
            let iWhite = NSString(format: "%02X", Int(fWhite * 255.0)) as String

            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            //let rgbw = (iRed << 24) + (iGreen << 16) + (iBlue << 8) + iWhite
            //return NSString(format: "%08X", rgbw) as String
            return iRed + iGreen + iBlue + iWhite
        } else {
            return "00000000"
        }
    }
}
