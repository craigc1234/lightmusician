//
//  BLEPeripheral.swift
//  DanceParty
//
//  Created by Craig Conner on 6/6/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import Foundation
import CoreBluetooth

enum ActionType: Int32 {
    case write = 1
    case respond = 2
    case read = 4
    case notify = 8
}

let kWriteResponseNotification = Notification.Name(rawValue: "WriteResponseNotification")
let kNoResponseNotification = Notification.Name(rawValue: "NoResponseNotification")

class BLEPeripheral: NSObject, CBPeripheralDelegate {

    // MARK: Variables
    var timeStamp = NSDate.timeIntervalSinceReferenceDate

    // This is for serial use right now, might want to make this an array
    /// The characteristic 0xFFE1 we need to write to, of the connectedPeripheral
    var writeCharacteristic: CBCharacteristic?

    /// UUID of the service to look for.
    var serialServiceUUID = CBUUID(string: "FFE0")

    /// UUID of the characteristic to look for.
    var serialCharacteristicUUID = CBUUID(string: "FFE1")

    //With response for the light
    /// Whether to write to the HM10 with or without response. Set automatically.
    /// Legit HM10 modules (from JNHuaMao) require 'Write without Response',
    /// while fake modules (e.g. from Bolutek) require 'Write with Response'.
    private var writeType: CBCharacteristicWriteType = .withResponse

    var peripheral: CBPeripheral
    var rssi: NSNumber?
    var advertisementKeyArray = [String]()
    var advertisementValueArray = [Any]()
    var connected = false
    var numServices = 0
    var hasPropertyWrite = false
    
    var maxMTUBuffer = 20

    var state = 0
    var servicesArray = [CBService]()
    var serviceToCharacteristicMap = [CBUUID: [CBCharacteristic]]()
    var serviceToCharacteristicDictMap = [CBUUID: [CBUUID: CBCharacteristic]]()

    init(initWithPeripheral newPeripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber ) {

        self.peripheral = newPeripheral
        super.init()
        self.peripheral.delegate = self

        rssi = RSSI
        //convert so I can index them later
        for (key, value) in advertisementData {
            advertisementKeyArray.append(key)
            advertisementValueArray.append(value)
        }
    }

    func startDiscoveringServices() {
        self.findMaxWriteLength(peripheral: self.peripheral)
       var services = [serialServiceUUID]
        if (getName()?.starts(with: "ZT"))! {
            serialServiceUUID = CBUUID(string: BLEPeripheralInterface.serviceIDText[0])
            services = [serialServiceUUID]
            serialCharacteristicUUID = CBUUID(string: BLEPeripheralInterface.characteristicIDText[0])
        }
        self.peripheral.discoverServices(services)
    }
    
    func startDiscoveringServices(serviceUUID: CBUUID) {
        let services = [serviceUUID]
        self.peripheral.discoverServices(services)
    }

    func getName() -> String? {
        return peripheral.name
    }

    func getID() -> UUID? {
        return peripheral.identifier
    }

    func getRSSI() -> NSNumber? {
        return rssi
    }

    func getADKeyArray() -> [String] {
        return advertisementKeyArray
    }

    func getADValueArray() -> [Any] {
        return advertisementValueArray
    }

    func getNumServices() -> Int? {
        if let services = peripheral.services {
            return services.count
        } else {
            return servicesArray.count
        }
    }

    func getService(index: Int) -> CBService? {
        if getNumServices() != nil && getNumServices()! > index {
            if let services = peripheral.services {
                return services[index]
            } else {
                //print("got service from array")
                return servicesArray[index]
            }
        } else {
            return nil
        }
    }

    func getCharacteristics(forService: CBService) -> [CBCharacteristic]? {
        //   print(forService.uuid)
        if let characteristics = serviceToCharacteristicMap[forService.uuid] {
            return characteristics
        } else {
            return []
        }
    }

    func getCharacteristic(forService: CBService, byIndex: Int) -> CBCharacteristic? {
        if let characteristics = serviceToCharacteristicMap[forService.uuid] {
            return characteristics[byIndex]
        } else {
            return nil
        }
    }

    func getCharacteristicByID(serviceID: CBUUID, charID: CBUUID) -> CBCharacteristic? {

        let characteristics = serviceToCharacteristicDictMap[serviceID]
        if characteristics != nil {
            return characteristics![charID]
        }

        return nil
    }

    //CBPeripheralDelegate

    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        if let error = error {
            print("error: \(error)")
            return
        }
        rssi = RSSI
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {

        if let error = error {
            print("error: \(error)")
            return
        }
        if peripheral != self.peripheral {
            // Wrong Peripheral
            return
        }

        if peripheral.services == nil || peripheral.services!.count == 0 {
            // No Services
            return
        }

        for service in peripheral.services! {
            if servicesArray.contains(service) == false {
                numServices += 1
                print("service \(service.description)")
                servicesArray.append(service)
                let charUUID = [serialCharacteristicUUID]
                peripheral.discoverCharacteristics(charUUID, for: service)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        numServices -= 1
        if peripheral != self.peripheral {
            // Wrong Peripheral
            return
        }

        if let error = error {
            print("error: \(error)")
            return
        }

        var charArray: [CBCharacteristic]?
        var charDict: [CBUUID: CBCharacteristic]?
        if let serviceMap = serviceToCharacteristicMap[service.uuid] {
            charArray = serviceMap
            charDict = serviceToCharacteristicDictMap[service.uuid]
        } else {
            charArray = [CBCharacteristic]()
            charDict = [CBUUID: CBCharacteristic]()
        }

        if let characteristics = service.characteristics {
             print("ch \(service.description)")
            for characteristic in characteristics { //where charDict?[characteristic.uuid] == nil {
                charArray!.append(characteristic)
                 print("char \(characteristic.description)")
                charDict?[characteristic.uuid] = characteristic
                //this doesn't seem to be consistent between Luxium and this???
                if serialServiceUUID == service.uuid && characteristic.properties.contains(.write) {
                    hasPropertyWrite = true
                }
            }
            serviceToCharacteristicMap[service.uuid] = charArray
            serviceToCharacteristicDictMap[service.uuid] = charDict

            serviceToCharacteristicMap[service.uuid] = charArray
            // Send notification that Bluetooth is connected and all required characteristics are discovered
            //all known services have been looked at
            if numServices == 0 {
                DispatchQueue.main.async {
                    self.findMaxWriteLength(peripheral: peripheral)
                    let blePeripheral = BLEManager.bleManager?.peripheralDict[peripheral.identifier]
                    blePeripheral?.connected = true
                    NotificationCenter.default.post(name: kCharacteristicsFoundNotification,
                                            object: blePeripheral, userInfo: nil)
                }
            }
        }
    }
    
    func findMaxWriteLength(peripheral: CBPeripheral) {
        let writeType: CBCharacteristicWriteType = (self.hasPropertyWrite == true) ? .withResponse : .withoutResponse
        maxMTUBuffer = peripheral.maximumWriteValueLength(for: writeType)
        print("maxMTU", maxMTUBuffer)
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        //write output - characteristic.value
        if let error = error {
            print("error: \(error)")
            return
        }

        let data = characteristic.value
        if data != nil {
            if let dataStr = String(data: data!, encoding: String.Encoding.utf8) {
                broadcastData(data: dataStr)
            }
        }
    }

    var responseString = ""

    func broadcastData(data: String) {
        responseString += data
        print(responseString)
        if responseString.contains("\n") {
            let trimmedString = responseString.components(separatedBy: .whitespacesAndNewlines).joined()
            BLEManager.bleManager?.bleQueue.itemCompleted(uuid: peripheral.identifier, response: trimmedString)
            
             //since itemCompleted sends the response to the callback, do we need to do this?
             let blePeripheral = BLEManager.bleManager?.peripheralDict[peripheral.identifier]
             DispatchQueue.main.async {
                NotificationCenter.default.post(name: kWriteOutputNotification,
                                                object: blePeripheral,
                                                userInfo: ["characteristic": self.writeCharacteristic as Any,
                                                           "data": trimmedString as Any])
            }
            
            responseString = ""
        }
    }

    //if using write with response, this gets called
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        if let error = error {
            print("error: \(error)")
            return
        }
        let blePeripheral = BLEManager.bleManager?.peripheralDict[peripheral.identifier]
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: kWriteResponseNotification, object: blePeripheral, userInfo: nil)
        }
    }

    func setWriteCharacteristic(service: CBUUID, characteristicID: CBUUID) {
        writeCharacteristic = serviceToCharacteristicDictMap[service]?[characteristicID]
    }
    
    func setWriteTrixyCharacteristic() {
        let serviceID = CBUUID(string: BLEPeripheralInterface.serviceIDText[0])
        let characteristicID = CBUUID(string: BLEPeripheralInterface.characteristicIDText[0])
        writeCharacteristic = serviceToCharacteristicDictMap[serviceID]?[characteristicID]
    }

    //Assume the first service/characteristic is the one to write to, if not specified
    func writeToFirstCharacteristic(value: String, type: ActionType) {
        let writeType: CBCharacteristicWriteType = (self.hasPropertyWrite == true) ? .withResponse : .withoutResponse
        let data = value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        if let characteristic = getService(index: 0)?.characteristics?[0] {
            if type == .respond {
                peripheral.setNotifyValue(true, for: characteristic)
            }
            peripheral.writeValue(data!, for: characteristic, type: writeType)
        }
    }

    func writeDataToCharacteristic(data: Data, type: ActionType) {
        let writeType: CBCharacteristicWriteType = (self.hasPropertyWrite == true) ? .withResponse : .withoutResponse
        if let characteristic = writeCharacteristic {
            if type == .respond {
                peripheral.setNotifyValue(true, for: characteristic)
            }
            peripheral.delegate = self
            peripheral.writeValue(data, for: characteristic, type: writeType)
        }
    }

    func writeDataToCharacteristic(characteristic: CBCharacteristic, data: Data, type: ActionType) {
        let writeType: CBCharacteristicWriteType = (self.hasPropertyWrite == true) ? .withResponse : .withoutResponse
         if type == .respond {
            peripheral.setNotifyValue(true, for: characteristic)
         }
        peripheral.delegate = self
        peripheral.writeValue(data, for: characteristic, type: writeType)
    }

    func readCharacteristic(characteristic: CBCharacteristic) {
        peripheral.readValue(for: characteristic)
    }

    func writeDescriptor(characteristic: CBCharacteristic) {
        //  peripheral.desc
    }

    func setNotify(characteristic: CBCharacteristic, notifyChar: CBCharacteristic, value: Bool) {
        let writeType: CBCharacteristicWriteType = (self.hasPropertyWrite == true) ? .withResponse : .withoutResponse
        peripheral.setNotifyValue(value, for: characteristic)
        var enableValue: UInt8 = value ? 1 : 0
        let enablyBytes = Data(bytes: &enableValue, count: MemoryLayout<UInt8>.size)
        peripheral.writeValue(enablyBytes, for: notifyChar, type: writeType)
    }

    func setNotifyByID(characteristicID: CBUUID, notifyCharID: CBUUID, sensorID: CBUUID, value: Bool) {
        let writeType: CBCharacteristicWriteType = (self.hasPropertyWrite == true) ? .withResponse : .withoutResponse
        let characteristic = (serviceToCharacteristicDictMap[sensorID]?[characteristicID])!
        let notifyChar = (serviceToCharacteristicDictMap[sensorID]?[notifyCharID])!
        peripheral.setNotifyValue(value, for: characteristic)
        var enableValue: UInt8 = value ? 1 : 0
        let enablyBytes = Data(bytes: &enableValue, count: MemoryLayout<UInt8>.size)
        peripheral.writeValue(enablyBytes, for: notifyChar, type: writeType)
    }
}
