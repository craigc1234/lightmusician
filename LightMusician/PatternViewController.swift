//
//  SensorTableViewController.swift
//  DanceParty
//
//  Created by Craig Conner on 6/6/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import UIKit
import CoreBluetooth

enum PatternType: Int {
    case solid = 0
    case rotating
    case twocolor
}

class PatternViewController: UITableViewController {
    
    var currentSettings: NSOrderedSet?
    var currentSettingIndex = 0
    var currentPatternIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = Color.darkBackground.value
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        currentPatternIndex = Int((currentSettings![currentSettingIndex] as? LightSettings)!.lightPattern)
        let currentSelectedIndexPath = IndexPath(row: currentPatternIndex, section: 0)
        let cell = tableView.cellForRow(at: currentSelectedIndexPath)
        cell?.backgroundColor = Color.darkBackground.value
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let currentSelectedIndexPath = IndexPath(row: currentPatternIndex, section: 0)
        var cell = tableView.cellForRow(at: currentSelectedIndexPath)
        cell?.backgroundColor = UIColor.white
        currentPatternIndex = indexPath.row
        (currentSettings![currentSettingIndex] as? LightSettings)!.lightPattern = Int16(indexPath.row)
        saveContext()
        cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = Color.darkBackground.value

        return nil
    }
}

extension PatternViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        let height = collectionView.bounds.height - 2
        let width = height
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCellID", for: indexPath)
        
        // Configure the cell
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.cornerRadius = 3
        let currentSetting = currentSettings![currentSettingIndex] as! LightSettings
        
        let color: ColorSet
        switch collectionView.tag {
        case 0:
            color = currentSetting.colors![0] as! ColorSet
        case 1:
            color = currentSetting.colors![indexPath.item] as! ColorSet
        default:
            let index = indexPath.item % 2
            color = currentSetting.colors![index] as! ColorSet
        }
        cell.backgroundColor = UIColor(red: CGFloat(color.red)/255.0, green: CGFloat(color.green)/255.0, blue: CGFloat(color.blue)/255.0, alpha: 1.0)

        return cell
    }
}
