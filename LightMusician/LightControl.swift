//
//  LightControl.swift
//  DanceParty
//
//  Created by Craig Conner on 6/6/17.
//  Copyright © 2017 LogicCascade. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

extension UIColor {
    
    func rgb() -> (red:Int, green:Int, blue:Int, alpha:Int)? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}

class LightControl {
    
    var _blePeripheral:BLEPeripheral?
    var _characteristic:CBCharacteristic?
    
    static var commandQueue = BLEQueue(queueWaitTime: 0.1, queueMaxCount: 1)
    
    static var numLights = 1
    static var program = false

    static var lightControl = LightControl()
    
    func controlLight() {
        
        switchColor()
    }
    
    func flashPeripheral(_ peripheral: BLEPeripheral) {
        peripheral.state = 0
        setFlashColor(peripheral)
        let _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            peripheral.state += 1
            
            if peripheral.state < 5 {
                self.setFlashColor(peripheral)
            } else {
                timer.invalidate()
            }
        }
    }
    
    func setFlashColor(_ peripheral: BLEPeripheral) {
        var greenValue = 0
        switch peripheral.state {
        case 0, 2, 4:
             greenValue = 255
        default:
            break
        }
        writeColorToLight(peripheral: peripheral, r: 0, g: greenValue, b: 0, w: 0)
    }
    
    func updateColor(lightSettings: LightSettings) {
        let isTrixy = lightSettings.numTrixyLights != 0

        lightSettings.nextColorIndex += 1
        if lightSettings.nextColorIndex >= lightSettings.numColors {
            lightSettings.nextColorIndex = 0
        }
        
        if isTrixy {
            updateTrixy(lightSettings: lightSettings)
        } else {
            updateLights(lightSettings: lightSettings)
        }
    }
    
    func updateLights(lightSettings: LightSettings) {
        var lightIndex = 0
        for lightObj in lightSettings.lights! {
            if let light = lightObj as? Light {
                let colorIndex = getLightColorIndex(lightSettings: lightSettings, lightIndex: lightIndex)
                let colorSet = lightSettings.colors![colorIndex] as? ColorSet
                
 
                if let peripheral = light.peripheral {
                    writeColorToLight(peripheral: peripheral, colorSet: colorSet!)
                }
            }
            lightIndex += 1
        }
    }

    
    func updateTrixy(lightSettings: LightSettings) {
        for lightObj in lightSettings.lights! {
            if let light = lightObj as? Light {
                if let peripheral = light.peripheral {
                    writeColorsToLightBox(peripheral: peripheral, lightSettings: lightSettings)
                }
            }
        }
    }
    
    func getLightColorIndex(lightSettings: LightSettings, lightIndex: Int) -> Int {
        let nextIndex = lightSettings.nextColorIndex
        let maxIndex = lightSettings.numColors
        let patternType = PatternType(rawValue: Int(lightSettings.lightPattern))!
        var colorIndex = 0
        switch patternType {
        case PatternType.solid:
            colorIndex = nextIndex
        case PatternType.rotating:
            colorIndex = (lightIndex + nextIndex) % Int(maxIndex)
        case PatternType.twocolor:
            let colorValue = lightIndex % 2
            colorIndex = nextIndex + colorValue
            if colorIndex >= maxIndex {
                colorIndex = 0
            }
        }
        return colorIndex
    }

    func fadeColors(lightSettings: LightSettings) {
        let isTrixy = lightSettings.numTrixyLights != 0
        
        if isTrixy {
            fadeTrixy(lightSettings: lightSettings)
        } else {
            fadeLights(lightSettings: lightSettings)
        }
    }
    
    func fadeLights(lightSettings: LightSettings) {
        var lightIndex = 0
        for lightObj in lightSettings.lights! {
            if let light = lightObj as? Light {
                let colorIndex = getLightColorIndex(lightSettings: lightSettings, lightIndex: lightIndex)
                let colorSet = lightSettings.colors![colorIndex] as? ColorSet
            
                if lightSettings.numFades > 0 {
                    let percentLeft = Double(lightSettings.fadeCountLeft) / Double(lightSettings.numFades)
                    let red = Int(Double(colorSet!.red) * percentLeft)
                    let blue = Int(Double(colorSet!.blue) * percentLeft)
                    let green = Int(Double(colorSet!.green) * percentLeft)
                    let white = Int(Double(colorSet!.white) * percentLeft)
                    if let peripheral = light.peripheral {
                        writeColorToLight(peripheral: peripheral, r: red, g: green, b: blue, w: white)
                    }
                }
            }
            lightIndex += 1
        }
    }
    
    func fadeTrixy(lightSettings: LightSettings) {
        for lightObj in lightSettings.lights! {
            if let light = lightObj as? Light {
                if let peripheral = light.peripheral {
                    writeColorsToLightBox(peripheral: peripheral, lightSettings: lightSettings, isFade: true)
                }
            }
        }
    }
    
    func switchColor() {
 /*       switch (SpectralViewController.colorScheme?.programType)! {
        case .continuous:
            sendContinueCommand()
        case .immediate:
            let colorValues = getCurrentColorValues(index: (SpectralViewController.colorScheme?.nextColorIndex)!)

            writeColorToLightBox(r: Int(colorValues[0]*255), g: Int(colorValues[1]*255), b: Int(colorValues[2]*255), w: 0)
        case .groovy:
            sendGroovyCommand()
        }*/
    }
    
 /*    func getCurrentColorValues(index: Int) -> [CGFloat] {
       let colorIndex = index % (SpectralViewController.colorScheme?.colorGradient.count)!
        let currentColor =  SpectralViewController.colorScheme?.colorGradient[colorIndex]
        
        var colorValues = currentColor?.cgColor.components
        for i in 0..<4 {
            if colorValues![i] > 1 {
                colorValues![i] = 1
            }
            
            if colorValues![i] < 0 {
                colorValues![i] = 0
            }
        }
        return colorValues!
    }*/
    
    static var lastString:String = ""
    //ints in range 0-255
    func writeColorToLight(peripheral: BLEPeripheral, colorSet: ColorSet) {
        var strValue = "M4000"
            
        strValue += String(format: "%02X", Int(colorSet.red))
        strValue += String(format: "%02X", Int(colorSet.green))
        strValue += String(format: "%02X", Int(colorSet.blue))
        strValue += String(format: "%02X", Int(colorSet.white))
        strValue += "\n"
  //      print(peripheral.getName()!, strValue)
        let data = strValue.data(using: String.Encoding.utf8)
        peripheral.writeDataToCharacteristic(data: data!, type: .write)
    }
    
    func writeColorToLight(peripheral: BLEPeripheral, r: Int, g: Int, b: Int, w: Int) {
        var strValue = "M4000"
        
        strValue += String(format: "%02X", r)
        strValue += String(format: "%02X", g)
        strValue += String(format: "%02X", b)
        strValue += String(format: "%02X", w)
        strValue += "\n"
//        print(peripheral.getName()!, strValue)
        let data = strValue.data(using: String.Encoding.utf8)
        peripheral.writeDataToCharacteristic(data: data!, type: .write)
    }
    
    func sendGroovyProgram() {
/*        let fade = "F00000A00\n"

        var newCommand = ""
        let initCommand = String(format: "G%d0\n", (SpectralViewController.colorScheme?.numColorsToUse)!)
        print(initCommand)
        newCommand += initCommand
        for colorNum in [Int](0..<(SpectralViewController.colorScheme?.numColorsToUse)!) {
    
            let lightCommand =  String(format: "E%02X", colorNum)
            let color = getCurrentColorValues(index: colorNum)
            var colorCommand = String(format: "%02X", Int(color[0] * 255.0))
            colorCommand += String(format: "%02X", Int(color[1] * 255.0))
            colorCommand += String(format: "%02X", Int(color[2] * 255.0))
            colorCommand += String(format: "%02X", 0)
            let command = lightCommand + colorCommand + "\n"
             newCommand += command
            print(command)
        }
        newCommand += fade
        print(fade)
            LightControl.lastString = newCommand
        let data = newCommand.data(using: String.Encoding.utf8)
        let element = Element(newPeripheral: _blePeripheral, newData: data!, newCallback: nil)
        LightControl.commandQueue.addElement(element)*/
    }
    
    func sendGroovyCommand() {
  /*      let newCommand = String(format: "G%d1\n", (SpectralViewController.colorScheme?.numColorsToUse)!)
        print(newCommand)
        let data = newCommand.data(using: String.Encoding.utf8)
        let element = Element(newPeripheral: _blePeripheral, newData: data!, newCallback: nil)
        LightControl.commandQueue.addElement(element)*/
    }
    
    func sendLightProgram() {
  /*      let fade = "f0000\n"
        let dwell = "d03E8\n"
        
        sendCommand(command: "B00\n")
        let numLights = LightControl.numLights
        for colorNum in [Int](0..<(SpectralViewController.colorScheme?.colorGradient.count)!) {
            for i  in [Int](0..<numLights) {
                let lightCommand =  String(format: "c%02X", (i + 1))
                let color = getCurrentColorValues(index: i + colorNum)
                var colorCommand = String(format: "%02X", Int(color[0] * 255.0))
                colorCommand += String(format: "%02X", Int(color[1] * 255.0))
                colorCommand += String(format: "%02X", Int(color[2] * 255.0))
                colorCommand += String(format: "%02X", 0)
                let command = lightCommand + colorCommand
                sendCommand(command: command)
            }
            sendCommand(command: fade)
            sendCommand(command: dwell)
        }
        sendCommand(command: "r\n")
        
        sendCommand(command: "X00\n")*/
    }

    func sendCommand(command: String) {
      //  print(command)
        if _characteristic != nil {
            LightControl.lastString = command
            let data = command.data(using: String.Encoding.utf8)
            _blePeripheral?.writeDataToCharacteristic(characteristic: _characteristic!, data: data!, type: .write)
        }
    }

    func sendContinueCommand() {
        if _characteristic != nil {
            let command = "Q\n"
            
            LightControl.lastString = command
            let data = command.data(using: String.Encoding.utf8)
            _blePeripheral?.writeDataToCharacteristic(characteristic: _characteristic!, data: data!, type: .write)
        }
    }
    
    func writeColorsToLightBox(peripheral: BLEPeripheral, lightSettings: LightSettings, isFade: Bool = false) {
        if let characteristic = peripheral.writeCharacteristic {
            let maxLoop = (Int(lightSettings.numTrixyLights) - 1) / 15
            for loopCount in [Int](0...maxLoop) {
                var newCommand = ""
                let startIndex = (loopCount * 15) + 1
                let endIndex = startIndex + 14 < Int(lightSettings.numTrixyLights) ? startIndex + 14 : Int(lightSettings.numTrixyLights)
                if startIndex <= endIndex {
                    for zoneIndex in [Int](startIndex...endIndex) {
                        let colorIndex = getLightColorIndex(lightSettings: lightSettings, lightIndex: zoneIndex)

                        let colorSet = lightSettings.colors![colorIndex] as! ColorSet
                        var red = Int(colorSet.red)
                        var blue = Int(colorSet.blue)
                        var green = Int(colorSet.green)
                        var white = Int(colorSet.white)

                        if isFade {
                            if lightSettings.numFades > 0 {
                                let percentLeft = Double(lightSettings.fadeCountLeft) / Double(lightSettings.numFades)
                                red = Int(Double(colorSet.red) * percentLeft)
                                blue = Int(Double(colorSet.blue) * percentLeft)
                                green = Int(Double(colorSet.green) * percentLeft)
                                white = Int(Double(colorSet.white) * percentLeft)
                            }

                        }
                        
                        let zoneString = String(format: "%02X", zoneIndex)
                        let rS = String(format: "%02X", red)
                        let gS = String(format: "%02X", green)
                        let bS = String(format: "%02X", blue)
                        let wS = String(format: "%02X", white)
                        let rgbwString = rS + gS + bS + wS
                        let prefix = "C"
                        
                        let command = prefix + zoneString + rgbwString + "\n"
                        newCommand += command
                        // command += "\n"
                    }
                }
               // LightControl.lastString = newCommand
                let data = newCommand.data(using: String.Encoding.utf8)
               // let element = Element(newPeripheral: _blePeripheral!, newData: data!, newCallback: nil)
               // LightControl.commandQueue.addElement(element)
                //     print(newCommand.count)
                peripheral.writeDataToCharacteristic(characteristic: characteristic, data: data!, type: .write)
            }
        }
    }
    
    func writeColorToLightBox(r: Int, g: Int, b: Int, w: Int) {
        if _characteristic != nil {
            let maxLoop = (LightControl.numLights - 1) / 15
            for loopCount in [Int](0...maxLoop) {
                var newCommand = ""
                let startIndex = (loopCount * 15) + 1
                let endIndex = startIndex + 14 < LightControl.numLights ? startIndex + 14 : LightControl.numLights
                for zoneIndex in [Int](startIndex...endIndex) {
                    let zoneString = String(format: "%02X", zoneIndex)
                    let rS = String(format: "%02X", Int(r))
                    let gS = String(format: "%02X", Int(g))
                    let bS = String(format: "%02X", Int(b))
                    let wS = String(format: "%02X", Int(w))
                    let rgbwString = rS + gS + bS + wS
                    let prefix = "C"
                    
                    let command = prefix + zoneString + rgbwString + "\n"
                    newCommand += command
                   // command += "\n"
                }
                LightControl.lastString = newCommand
                let data = newCommand.data(using: String.Encoding.utf8)
                let element = Element(newPeripheral: _blePeripheral!, newData: data!, newCallback: nil)
                LightControl.commandQueue.addElement(element)
           //     print(newCommand.count)
           //     _blePeripheral.writeDataToCharacteristic(characteristic: _characteristic!, data: data!, type: .write)
            }
        }
    }
    
    static func toRGBW(_ color: UIColor) -> String {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        var fWhite: CGFloat = 0
        if color.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            fRed = max(fRed, 0.0)
            fGreen = max(fGreen, 0.0)
            fBlue = max(fBlue, 0.0)
            fAlpha = max(fAlpha, 0.0)
            fWhite = max(fWhite, 0.0)
            let iRed = NSString(format: "%02X", Int(fRed * 255.0)) as String
            let iGreen = NSString(format: "%02X", Int(fGreen * 255.0)) as String
            let iBlue = NSString(format: "%02X", Int(fBlue * 255.0)) as String
            // let iAlpha = NSString(format: "%02X", Int(fAlpha * 255.0)) as String
            let iWhite = NSString(format: "%02X", Int(fWhite * 255.0)) as String
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            //let rgbw = (iRed << 24) + (iGreen << 16) + (iBlue << 8) + iWhite
            //return NSString(format: "%08X", rgbw) as String
            return iRed + iGreen + iBlue + iWhite
        } else {
            return "00000000"
        }
    }
    
    func getRandom() -> String {
        let random = arc4random_uniform(256)
        return String(format:"%02X", random)
    }
}
