//
//  ConnectionDialogs.swift
//  LightMusician
//
//  Created by Aardvark on 2/16/19.
//  Copyright © 2019 LogicCascade. All rights reserved.
//

import Foundation
import UIKit

protocol ConnectionDelegate: class {
    func changeConnectionState(newState: Bool)
}

protocol ConnectionDialogDelegate: class {
    func showConnectingDialogs(hasBluetooth: Bool)
    func dialogDismissed()
    func ignoreChosen()
    func tryAgainChosen()
}

var connectingIsShowing = false
extension UIViewController {
    
    func showConnectingDialog() {
        
        let connectingDialog = UIAlertController(title: "Connecting...", message: "",
            preferredStyle: UIAlertController.Style.alert)
        if connectingIsShowing == false {
            connectingIsShowing = true
            self.present(connectingDialog, animated: true) {
                self.perform(#selector(self.dismissAlertController(alertController:)), with: connectingDialog,
                             afterDelay: 2.0)
            }
        }
    }
    
    @objc internal func dismissAlertController(alertController: UIAlertController?) {
        alertController?.dismiss(animated: true) {
            connectingIsShowing = false
            BLEPeripheralInterface.peripheralInterface?.dialogDelegate?.dialogDismissed()
        }
    }
    
    func doConnectedDialog() {
        if let box = AppDelegate.rootDataObject?.trixySettings?[0] as? Light {
            if let name = box.name {
                let connectedDialog = UIAlertController(title: "Connected", message: "Connected to \(name)",
                    preferredStyle: UIAlertController.Style.alert)
                
                connectedDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_ :
                    UIAlertAction!) -> Void in
          //          self.dismissSelf()
                }))
                
                self.present(connectedDialog, animated: true, completion: nil)
            }
        }
    }
    
    func showDisconnectedDialog() {
        let connectedDialog = UIAlertController(title: "Disconnected", message: "Rescan from the Configure view.",
                                                                          preferredStyle: UIAlertController.Style.alert)
        connectedDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_ :
            UIAlertAction!) -> Void in
       //     self.dismissSelf()
        }))
        
        self.present(connectedDialog, animated: true, completion: nil)
    }

    func showNoBluetoothPopup(dialogDelegate: ConnectionDialogDelegate) {
        if BLEManager.bleManager?.centralManager?.state == .poweredOff ||
            BLEManager.bleManager?.centralManager?.state == .unsupported {
            let alert = UIAlertController(title: "Bluetooth is Off", message:
                "The app requires Bluetooth to be set to On to control lights.",
                                          preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ignore", style: .cancel, handler: { (_ :
                UIAlertAction!) -> Void in
                dialogDelegate.ignoreChosen()
            }))
            alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { (_ :
                UIAlertAction!) -> Void in
                dialogDelegate.tryAgainChosen()
                BLEPeripheralInterface.peripheralInterface!.doConnection()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNoPeripheralPopup(_ dialogDelegate: ConnectionDialogDelegate? = nil) {
        let alert = UIAlertController(title: "No Peripherals Found", message:
            "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ignore", style: .cancel, handler: { (_ :
            UIAlertAction!) -> Void in
            dialogDelegate?.ignoreChosen()
        }))
        alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { (_ :
            UIAlertAction!) -> Void in
            dialogDelegate?.tryAgainChosen()
            BLEPeripheralInterface.peripheralInterface!.doConnection()
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}

