//
//  AnimationLaunchViewController.swift
//  LuxTrix
//
//  Created by Aardvark on 8/13/18.
//  Copyright © 2018 Luxapel. All rights reserved.
//

import UIKit
import CoreBluetooth

class AnimationLaunchViewController: UIViewController, ConnectionDialogDelegate {
    
    var connectingDialog: UIAlertController?
    var connectingDialogTime = 2.0
    weak var connectionDelegate: ConnectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BLEManager.peripheralIDArray = BLEPeripheralInterface.peripheralIDArray
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        BLEPeripheralInterface.peripheralInterface!.dialogDelegate = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        BLEPeripheralInterface.peripheralInterface!.dialogDelegate = self
        doConnection()
    }
    
    func doConnection() {
        BLEPeripheralInterface.peripheralInterface?.doConnection()
    }
    
    func showConnectingDialogs(hasBluetooth: Bool) {
        if hasBluetooth {
            showConnectingDialog()
        } else {
            showNoBluetoothPopup(dialogDelegate: self)
        }
    }
    
    func dialogDismissed() {
        if BLEManager.bleManager?.peripheralArray.count == 0 {
            showNoPeripheralPopup(self)
        } else {
            self.dismissSelf()
        }
    }
    
    func ignoreChosen() {
        self.dismissSelf()
    }
    
    func tryAgainChosen() {
        doConnection()
    }

    @objc
    func dismissSelf() {
        performSegue(withIdentifier: "showMainViewID", sender: nil)
    }
    
    func testPeripheralCount() {
        let peripheralCount = BLEManager.bleManager?.peripheralArray.count
        if peripheralCount == 0 {
            showNoPeripheralPopup(self)
        } else {
            self.dismissSelf()
        }
        
    }
}
