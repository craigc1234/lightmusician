# README #

This project is an ongoing test case to sync light changes to music. Not directly connected to the beat, 
but to points of emphasis in the music.

### How does it work? ###

It uses a FFT to track the magnitude of incoming sound, and then sums that magnitude for each band. When the magnitude
sum drops by a given threshold, the light changes to the next specified color.

### Really quick overview: ###

In SpectralViewController, the function gotSomeAudio is the heart of the music tracking. The rest is window dressing.
Since this is an ongoing test project, don't be surprised to see lines commented out, and functions not currently called.

### Thanks to: ###

Ooper - [aurioTouch](https://github.com/ooper-shlab) example Swift translation

Vicc Alexander - [Chameleon](https://github.com/ViccAlexander/Chameleon)

Joel Teply [HSBColorPicker](https://stackoverflow.com/questions/27208386/simple-swift-color-picker-popover-ios)

Brian Corbin - [RangeSlider](https://github.com/BrianCorbin/SwiftRangeSlider)

For further info:

craigc1234@comcast.net